import $ from 'jquery';
import MN from './modules/nav.Main.js';
import P from './modules/bg.Parallax.js';
import SA from './modules/scrollAnimate.js';
import MC from './modules/main.Content.js';
import UST from './modules/uppercaseSectionTitles.js';
import AC from './modules/Accordion.js';
import CS from './modules/CaseStudy.js';
import IS from './modules/module.ImageShowcase.js';
import CF from './modules/form.Contact.js';
import S from './modules/module.Slider.js';
import PA from './modules/grid.PostArchive.js';
import STF from './modules/module.StretchToFold.js';
import NS from './modules/slider.News.js';
import IL from './modules/module.ImageList.js';

window.$ = $;

var APP = {
	modules : [
		MN,
		P,
		SA,
		MC,
		UST,
		AC,
		CS,
		IS,
		CF,
		S,
		PA,
		STF,
		NS,
		IL
	],
	_init : () => {
		APP.modules.forEach( module => module._init() );
	},
};

APP._init();

window.APP = APP;