import $ from 'jquery';

let CF = {
	_init : () => {
		$(document).ready(CF._ready);
	},
	_ready : () => {
		$('.contactform-form').on('submit', CF._submitHandler);
	},
	_submitHandler : (e) => {
		e.preventDefault();

		let formData = $('.contactform-form').serializeArray();
		let submitData = {
			action : 'mc_contact_form_submit'
		};

		formData.forEach( row => submitData[row.name] = row.value );

		$.post(ajax_url, submitData, (res, status, xhr) => {
			console.log(res, status);
			if( status == 'success' ){
				try {
					let jsonres = JSON.parse(res);
					/*
					{
						status: number (0 = bad, 1 = warning, 2 = good),
						message: string
					}
					 */
					if( jsonres.status == 2 ){
						$('.contactform-form').remove();
						$('.contactform-header').after(
								`<div class="contactform-message contactform-message--${jsonres.status}">${jsonres.message}</div>`
							);
					}
				}
				catch(e) {
					console.log(e);
				}
			}
			else{
				return;
			}
		});
	}
};

module.exports = CF;