import $ from 'jquery';
import Masonry from 'masonry-layout';
import jQueryBridget from 'jquery-bridget';
import imagesLoaded from 'imagesloaded';

jQueryBridget('masonry', Masonry, $);

imagesLoaded.makeJQueryPlugin($);

/**
 * [CS handles case study grid seemore shit, case study single img full shit & the case study masonry shit]
 * @type {Object}
 */
let CS = {
	/**
	 * [_init entry point]
	 */
	_init : () => {
		// call ready when ready
		$(document).ready(CS._ready);
	},
	/**
	 * [_ready fired when document is ready]
	 */
	_ready : () => {
		// store the casestudygrid instances
		CS.caseStudyGrids = $('.casestudygrid');
		// were there any?
		if( CS.caseStudyGrids.length ){
			// if so then loop through them and build SeeMore objects
			CS.caseStudyGrids.each( (i, el) => {let sm = new CS.SeeMore(el); sm._init(); } );
		}
		// store the casestudygrid full images
		CS.fullImages = $('.single-mc_casestudy .main-content-content img.full');
		// are there any?
		if( CS.fullImages.length ){
			// loop through each fullimage
			CS.fullImages.each( (i, el) => {
				// add the wrap
				$(el).wrap('<div class="fullwrap"></div>');
			} );
		}

		if( ( CS.masonry = $('.single-mc_casestudy .main-content-content-masonry') ).length > 0 ){
			let grid = CS.masonry.masonry({
			    itemSelector: ".single-mc_casestudy .main-content-content-masonry-item",
			    // columnWidth: ".single-mc_casestudy .main-content-content-masonry-item",
			    // gutter : 40,
			    stamp : '.single-mc_casestudy .main-content-content-masonry-quotes',
			    // percentPosition: true,
			});
			grid.imagesLoaded().progress(function(){
			    grid.masonry('layout');
			}).done(function(){
			    grid.masonry('layout');
			});
			window.grid = grid;
			setTimeout(() => {
			    window.grid.masonry('layout');
			}, 2000);
		}
	},
	/**
	 * [SeeMore the object that handles the seemore aspect of the ui]
	 * @param {HTMLElement|jQuery} container the casestudygrid
	 */
	SeeMore : function(container) {
		/**
		 * [_init entry point]
		 */
		this._init = function(){
			// store vars
			this.container = container.constructor.name == 'jQuery' ? container : $(container);
			this.grid = $('.casestudygrid-wrapper-grid', this.container);
			this.seemore = $('.casestudygrid-wrapper-seemore', this.container);
			this.csData = window[this.container.data('scriptid')];
			// listen for click on seemore
			this.seemore.on('click', this._handleSeeMore.bind(null, this));
		};
		/**
		 * [_handleSeeMore adds more items to the grid/deletes the button when no more are left]
		 * @param  {object} context this arg essentially
		 * @param  {object} e       the event object
		 */
		this._handleSeeMore = function(context, e){
			// get the num of posts already displayed
			let index = $('.casestudygrid-wrapper-grid-item', context.container).length;
			// get the posts to display
			let posts = context.csData.slice(index, index + 4);
			// did that work?
			if( posts.length ){
				// then loop through all the posts to make
				posts.forEach( (post) => {
					// and append the post to the grid using the post's data
					context.grid.append(
						`
						<div data-aos="fade-in" class="casestudygrid-wrapper-grid-item">
							<div class="casestudygrid-wrapper-grid-item-text">
								<div class="casestudygrid-wrapper-grid-item-text-logocontainer">
									<img src="${post.image}" class="casestudygrid-wrapper-grid-item-text-logocontainer-logo">
								</div>
								<div class="casestudygrid-wrapper-grid-item-text-excerpt">${post.excerpt}</div>
								<a href="${post.permalink}" class="casestudygrid-wrapper-grid-item-text-link">learn more</a>
							</div>
							<div class="casestudygrid-wrapper-grid-item-fade"></div>
							<div class="casestudygrid-wrapper-grid-item-titlecontainer">
								<div class="casestudygrid-wrapper-grid-item-titlecontainer-title">${post.title}</div>
							</div>
							<div style="background-image: url('${post.bg}')" class="casestudygrid-wrapper-grid-item-bgimage"></div>
						</div>
						`
					);
				} );
			}
			// if the num of grid items is the same as the number of items in the csData array
			if( $('.casestudygrid-wrapper-grid-item', context.container).length === context.csData.length ){
				// then remove the seemore button
				context.seemore.remove();
			}
		};
	},
};

module.exports = CS;