import $ from 'jquery';
import { debounce } from './misc.js';

let MainNav = {
	_init : () => {
		$(document).ready(MainNav._ready);
	},
	_ready : () => {
		MainNav.toggle = $('.mainnav-wrapper-toggle');
		MainNav.hamburger = $('.mainnav-wrapper-toggle .hamburger');

		if( MainNav.hamburger.length > 0 ){
			MainNav.hamburger.on('click', MainNav._toggleClickHandler);
		}

		$(window).on('scroll', () => {
			if( MainNav.toggle.hasClass('is-active') && $(window).scrollTop() > 0 ){
				MainNav.toggle.removeClass('is-active');
				MainNav.hamburger.removeClass('is-active');
			}
		});
	},
	_toggleClickHandler : (e) => {
		MainNav._toggleMobileNav(e);
	},
	_toggleMobileNav : (e) => {
		$(e.target).toggleClass('is-active');
		MainNav.toggle.toggleClass('is-active');
		if( MainNav.hasClass('is-active') ){
			$('html body').animate({
				scrollTop : 0,
			})
		}
	},
};

module.exports = MainNav;