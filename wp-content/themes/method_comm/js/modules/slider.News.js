import Swiper from 'swiper';

let NS = {
	_init : () => {
		$(document).ready(NS._ready);
	},
	_ready : () => {
		NS.sliders = $('.newsslider');
		NS.sliderObjects = [];
		if( NS.sliders.length > 0 ){
			NS.sliders.each( (index, slider) => {
				let sliderObject = new NS.Slider(slider);
				sliderObject._init();
			} );
		}
	},
	Slider : function(slider){
		this.context = slider;
		this.container = $('.swiper-container', slider);
		this.swiper = undefined;
		this._init = function(){
			this.swiper = new Swiper(this.container, {
				navigation: {
					nextEl: '.swiper-button-next',
					prevEl: '.swiper-button-prev',
				},
				on: {
					click: function(e) {
						if( $(e.target).hasClass('newsslider-wrapper-slider-wrapper-slide') ){
							window.open(e.target.dataset.url);
						}
					}
				}
			});
		};
	}
};

export default NS;