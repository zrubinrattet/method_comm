import $ from 'jquery';

let UST = {
	_init : () => {
		$(document).ready(UST._ready);
	},
	_ready : () => {

		UST.titles = $('.section-wrapper-title');

		if( UST.titles.length > 0 ){
			$.each(UST.titles, (i, el) => {
				if( i > 0 ){
					$(el).addClass('section-wrapper-title--caps');
				}
			});
		}
	}
};

module.exports = UST;