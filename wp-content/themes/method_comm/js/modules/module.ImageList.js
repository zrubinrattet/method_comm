let IL = {
	imageLists : [],
	_init : () => {
		$(document).ready(IL._ready);
	},
	_ready : () => {
		IL.sections = $('.imagelistmodule');

		if( IL.sections.length > 0 ){
			IL.sections.each( (index, section) => {
				let imageList = new IL.ImageList(section);
				IL.imageLists.push(imageList);
				imageList._init();
			} );
		}
	},
	ImageList : function(section){
		this.context = section;
		this.fc_index = this.context.dataset.fc_index;
		this._init = function(){
			this.imagesData = window['imagelistmodule' + this.fc_index];
			this.imagesContainer = $('.imagelistmodule-wrapper-images', this.context);
			this.seemoreButton = $('.imagelistmodule-wrapper-seemore', this.context);
			// if a seemorebutton is found & also if there's more than 2 images
			if( this.seemoreButton.length > 0 ){

				// render 2 images
				this.imagesData.forEach( (image, index) => {
					if( index < 2 ){
						this.imagesContainer.append('<img class="imagelistmodule-wrapper-images-image" src="' + image.url + '">');
					}
				} );

				let that = this;
				this.seemoreButton.on('click', this._seemoreButtonClickHandler.bind(null, that));
			}
		};
		this._seemoreButtonClickHandler = function(that, e){
			e.preventDefault();

			let imagesCount = $('.imagelistmodule-wrapper-images-image', that.context).length;

			that.imagesData.forEach( (image, index) => {
				// only add two at a time
				if( index >= imagesCount && index < imagesCount + 2 ){
					that.imagesContainer.append('<img class="imagelistmodule-wrapper-images-image" src="' + image.url + '">');
				}
			} );

			// if there's no more images to get
			if( $('.imagelistmodule-wrapper-images-image', that.context).length === that.imagesData.length ){
				// hide the button
				that.seemoreButton.hide();
			}
		}
	}
};

export default IL;