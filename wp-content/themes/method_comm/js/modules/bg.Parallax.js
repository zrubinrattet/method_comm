import $ from 'jquery';
import {scalemap} from './misc.js';

let P = {
	/**
	 * [strength how strong is parallax]
	 * @type {Number}
	 */
	strength : 3,
	/**
	 * [_init entry point]
	 */
	_init : function(){
		// when parallax-bg has loaded
		$.when($('.parallax-bg')).then( () => {
			// show the parallax container
			$('.parallax').show();
			// store the bg
			P.bg = $('.parallax-bg');
			// fire the positioning func
			P._resizeLoadScrollHandler();
			// set event listeners for resize & scroll on window
			$(window).on('resize scroll', P._resizeLoadScrollHandler);
		} );
	},
	/**
	 * [_getAmount gets the amount to translateY by]
	 * @return {string} the amount to translateY as a percentage & string
	 */
	_getAmount : function(){
		var scale = scalemap($(window).scrollTop(), -400, 89, 0, P.strength);
		return 0 + scale + '%';
	},
	/**
	 * [_resizeLoadScrollHandler callback to set the bg transform]
	 */
	_resizeLoadScrollHandler : function(){
		P.bg.css('transform', 'translate3d(0%, ' + P._getAmount() + ',0)');
	}
}

module.exports = P;