import $ from 'jquery';
import Swiper from 'swiper';

let S = {
	defaultParams : {

	},
	swipers : [],
	_init : () => {
		$(document).ready(S._setupSwipers);
	},
	_setupSwipers : () => {
		$('.slidermodule-wrapper-container').each( (index, element) => {
			let args = $.extend(
						true,
						S.defaultParams,
						window['slidermodule' + $(element).parents('.slidermodule').data('index') + 'params']
					);
			S.swipers.push(
				new Swiper(
					element,
					args
				)
			);
		} );
	}
};

module.exports = S;