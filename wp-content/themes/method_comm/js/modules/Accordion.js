import $ from 'jquery';

let A = {
	speed : 250,
	_init : () => {
		$(document).ready(A._ready);
	},
	_ready : () => {
		A.toggles = $('.accordion-wrapper-row-togglecontainer-toggle');
		A.contents = $('.accordion-wrapper-row-content');

		if( A.toggles.length ){
			A.toggles.on('click', A._toggleClickHandler);
		}
	},
	_toggleClickHandler : (e) => {
		A.contents.slideUp(A.speed);
		A.toggles.removeClass('active');
		let content = $(e.target).parents('.accordion-wrapper-row').find('.accordion-wrapper-row-content');
		if( content.css('display') == 'none' ){
			content.animate({
				opacity: 'toggle',
				height: 'toggle',
			}, A.speed);
			$(e.target).addClass('active');
		}
	},
};

module.exports = A;