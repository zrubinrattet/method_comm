import $ from 'jquery';
import Swiper from 'swiper';

let IS = {
	swipers : [],
	_init : () => {
		$.when($('.imageshowcase')).then(IS._initSwiper);
	},
	_initSwiper : () => {
		if( $('.imageshowcase-wrapper').length ){
			$('.imageshowcase-wrapper').each( (i, el) => {
				IS.swipers.push( new Swiper(el, {
					speed: 2000,
					slidesPerView: 2,
					slidesPerColumn: 2,
					spaceBetween: 20,
					breakpoints: {
						576: {
							slidesPerView: 3,
							slidesPerColumn: 2,
						},
						768: {
							slidesPerView: 4,
							slidesPerColumn: 2,
						},
						992: {
							slidesPerView: 5,
							slidesPerColumn: 2,
						}
					},
					loop: true,
					freeMode: true,
				}) );
			} );
			// listen for scroll events
			$(window).on('scroll resize', IS._scrollHandler);
			// fire to set up
			IS._scrollHandler();
		}
	},
	_scrollHandler : (e) => {
		IS.swipers.forEach( (swiper) => {
			swiper.translateTo( ($(window).scrollTop() * -1) / 10, 0 );
		} );
	},
};

module.exports = IS;