import $ from 'jquery';

let MC = {
	_init : () => {
		$.when($('.hero-small, .footer')).then( () => {
			$('.main-content').css('minHeight', 'calc(100vh - ' + ($('.hero-small').height() + $('.footer').height()) + 'px)' );
		} );
	},
};

module.exports = MC;