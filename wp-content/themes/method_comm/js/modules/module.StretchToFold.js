import {debounce} from './misc.js';

let STF = {
	_init : () => {
		// $(document).ready(STF._windowResizeHandler);
		$(window).on('load resize', debounce(STF._windowResizeHandler, 250));
	},
	_windowResizeHandler : () => {
		STF.container = $('.stretchtofold');
		// if container exists
		if( STF.container.length > 0 ){
			// resize module height to stretch to the fold if on desktop on mobile but not tablet
			if( $(window).width() > 992 ){
				if( $(window).height() > STF.container.outerHeight() + STF.container.offset().top ){
					STF.container.height( STF.container.height() + ( $(window).height() - ( STF.container.height() + STF.container.offset().top ) ) );
				}
			}
			else{
				// remove the height attr in case someone resizes the screen
				STF.container.css('height', '');
			}
		}
	}
};

export default STF;