import $ from 'jquery';
import AOS from 'aos';

let SA = {
	things : undefined,
	_init : () => {
		$(document).ready(() => {
			AOS.init({
				delay: 200
			});
		});
	},
};

module.exports = SA;