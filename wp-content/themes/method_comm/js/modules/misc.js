/**
 * [_map amazing map function. remaps a range of numbers to another range based on high & low posts of two sets]
 * @param  {number} v     the value to map
 * @param  {number} e     the low point of the value
 * @param  {number} g     the high point of the value
 * @param  {number} a     the low point of the new value
 * @param  {number} n     the high point of the new value
 * @return {number}       the mapped value
 */
export function	map(v, e, g, a, n){
	let val = a + (v - e) * (n - a) / (g - e);
	if( a < n ){
		if( val < a ){
			return a;
		}
		else if( val > n ){
			return n;
		}
		else{
			return val;
		}
	}
	else{
		if( val < n ){
			return n;
		}
		else if( val > a ){
			return a;
		}
		else{
			return val;
		}
	}
}

/**
 * [scalemap similar to above but no limits]
 * @param  {number} v     the value to map
 * @param  {number} e     the low point of the value
 * @param  {number} g     the high point of the value
 * @param  {number} a     the low point of the new value
 * @param  {number} n     the high point of the new value
 * @return {number}       the mapped value
 */
export function scalemap(v, e, g, a, n){
	return (v - e) * (n - a) / (g - e) + a;
}

// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
//
// example usage:
// var myEfficientFn = debounce(function() {
// 	// All the taxing stuff you do
// }, 250);
//
// window.addEventListener('resize', myEfficientFn);
export function debounce(func, wait = 200, immediate){
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};