let PA = {
	_init : () => {
		$(document).ready(PA._ready);
	},
	_ready : () => {
		if( $('.postarchive-wrapper-seemore').length ){
			$('.postarchive-wrapper-seemore').on('click', PA._handleSeeMore);
		}
	},
	_handleSeeMore : () => {
		let data = {
			action : 'postarchive_seemore',
			offset : $('.postarchive-wrapper-grid-item').length,
		};
		// separate the post archive from the other queries
		if( typeof isPostArchive != 'undefined' ){
			data.postArchive = true;
		}
		// if the queried object is expressed
		if( typeof qo != 'undefined' ){
			// if author
			if( typeof qo.data != 'undefined' && typeof qo.data.ID != 'undefined' ){
				data.author = qo.data.ID;
			}
			// if category
			if( typeof qo.cat_ID != 'undefined' ){
				data.cat_id = qo.cat_ID;
			}
		}

		$.post(ajax_url, data, (res, status, xhr) => {
			$('.postarchive-wrapper-grid').append(res);
			if( $('.postarchive-wrapper-totalposts').val() == $('.postarchive-wrapper-grid-item').length ){
				$('.postarchive-wrapper-seemore').remove();
			}
		});
	},
}

export default PA;