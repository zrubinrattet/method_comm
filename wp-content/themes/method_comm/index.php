<?php 

get_header();

ComponentUtil::get('nav.Main');

PartialUtil::get('hero.Main');
?>
<div class="main">
	<div class="main-content">
		<?php echo apply_filters( 'the_content', do_shortcode($post->post_content) ); ?>
	</div>
	<?php
		if( get_field('disable_parallelogram', $post->ID) == false ){
			PartialUtil::get('bg.Parallax');
		}
	?>
</div>
<?php
get_footer();
?>