<?php 

require 'vendor/autoload.php';

require 'src/handlers/class.ACFHandler.php';
require 'src/class.SetupTheme.php';

require 'src/utils/class.LoaderUtil.php';
require 'src/utils/class.MiscUtil.php';
require 'src/utils/class.ComponentUtil.php';
require 'src/utils/class.PartialUtil.php';

require 'src/class.Layout.php';
require 'src/class.ModuleBuilder.php';

?>