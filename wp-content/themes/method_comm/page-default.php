<?php
/* Template Name: Method Module Builder */

get_header();

ComponentUtil::get('nav.Main');
// load the big 
PartialUtil::get('hero.Main');

$mb = new ModuleBuilder();

if( $mb->has_modules() ){
	echo '<div class="main">';
		if( !empty( $custom_css = get_field('template_custom_css', $post->ID) ) ){
			?>
			<style type="text/css">
				<?php echo $custom_css; ?>
			</style>
			<?php
		}
		$mb->render();
		if( get_field('disable_parallelogram', $post->ID) == false ){
			PartialUtil::get('bg.Parallax');
		}
	echo '</div>';
}

get_footer();

?>