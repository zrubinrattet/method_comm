<?php

get_header();

ComponentUtil::get('nav.Main');

PartialUtil::get('hero.Main');

?>
<div class="singlehead" data-aos="fade-in">
	<div class="singlehead-wrapper">
		<h1 class="singlehead-wrapper-title"><?php echo $post->post_title; ?></h1>
		<div class="singlehead-wrapper-excerpt"><?php echo $post->post_excerpt; ?></div>
		<hr class="singlehead-wrapper-divider">
		<div class="singlehead-wrapper-authorname"><?php echo get_the_author_meta('display_name', $post->post_author); ?></div>
		<div class="singlehead-wrapper-authortitle"><?php the_field('user_title', 'user_' . $post->post_author); ?></div>
		<div class="singlehead-wrapper-postdate"><?php echo date('F j, Y', strtotime($post->post_date)); ?></div>
		<img src="<?php echo get_field('post_single_image', $post->ID)['url'] ?>" class="singlehead-wrapper-image">
		<div class="singlehead-wrapper-attribution"><?php the_field('post_attribution', $post->ID); ?></div>
	</div>
</div>
<div class="singlebody" data-aos="fade-in">
	<div class="singlebody-wrapper">
		<div class="singlebody-wrapper-content">
			<div class="singlebody-wrapper-content-body">
				<?php echo wpautop(apply_filters('the_content', do_shortcode($post->post_content))); ?>
			</div>
			<div class="singlebody-wrapper-content-author">
				<img src="<?php echo get_field('user_image_1', 'user_' . $post->post_author)['url'] ?>" class="singlebody-wrapper-content-author-image">
				<div class="singlebody-wrapper-content-author-bio"><?php the_field('user_bio_alt', 'user_' . $post->post_author); ?></div>
				<div class="singlebody-wrapper-content-author-social">
					<div class="singlebody-wrapper-content-author-social-wrap">
						<h6 class="singlebody-wrapper-content-author-social-wrap-title">Share</h6>
						<div class="singlebody-wrapper-content-author-social-wrap-icons">
							<a target="_blank" href="https://twitter.com/intent/tweet?url=<?php the_permalink($post->ID); ?>" class="singlebody-wrapper-content-author-social-wrap-icons-link">
								<img src="<?php echo get_template_directory_uri(); ?>/lib/img/birdie-black.png" class="singlebody-wrapper-content-author-social-wrap-icons-link-image">
							</a>
							<a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink($post->ID); ?>" class="singlebody-wrapper-content-author-social-wrap-icons-link">
								<img src="<?php echo get_template_directory_uri(); ?>/lib/img/in-black.png" class="singlebody-wrapper-content-author-social-wrap-icons-link-image">
							</a>
							<a href="mailto:?subject=Method%20Communications%20::%20<?php echo rawurlencode($post->post_title); ?>&body=Check%20out%20this%20great%20blog%20post%20from%20Method%20Communications:%0D%0A%0D%0A<?php echo rawurlencode(get_permalink($post->ID)); ?>" class="singlebody-wrapper-content-author-social-wrap-icons-link">
								<img src="<?php echo get_template_directory_uri(); ?>/lib/img/email-black.png" class="singlebody-wrapper-content-author-social-wrap-icons-link-image">
							</a>
						</div>
					</div>
				</div>
			</div>
			<?php
			$cats = wp_get_object_terms( $post->ID, 'category' );
			if( !empty($cats) ): ?>
				<div class="singlebody-wrapper-content-topics">
					<h6 class="singlebody-wrapper-content-topics-title">topics</h6>
					<ul class="singlebody-wrapper-content-topics-cats">
						<?php foreach( $cats as $index => $cat ): ?>
							<li class="singlebody-wrapper-content-topics-cats-cat">
								<?php
									$catlink = '<a class="singlebody-wrapper-content-topics-cats-cat-link" href="' . get_term_link( $cat, 'category' ) . '">' . $cat->name . '</a>';
									if( $index == 0 ){
										echo $catlink;
									}
									elseif( $index < count($cats) - 1 ){
										echo ', ' . $catlink;
									}
									else{
										echo '&nbsp;&amp; ' . $catlink;
									}
								?>
							</li>
						<?php endforeach; ?>
					</ul>
				</div>
			<?php endif; ?>
		</div>
		<div class="singlebody-wrapper-relatedposts">
			<h2 class="singlebody-wrapper-relatedposts-title">MORE</h2>
			<?php
			$posts = get_posts(array(
				'post_type' => 'post',
				'posts_per_page' => 3,
				'exclude' => $post->ID,
				'post_status' => 'publish',
				'orderby' => 'rand',
			));
			?>
			<ul class="singlebody-wrapper-relatedposts-posts">
				<?php foreach( $posts as $post ): ?>
					<li class="singlebody-wrapper-relatedposts-posts-post">
						<div class="singlebody-wrapper-relatedposts-posts-post-text">
							<a href="<?php the_permalink($post->ID); ?>" class="singlebody-wrapper-relatedposts-posts-post-text-title"><?php echo $post->post_title ?></a>
							<a href="<?php echo get_author_posts_url( $post->post_author ); ?>" class="singlebody-wrapper-relatedposts-posts-post-text-author"><?php echo get_the_author_meta( 'display_name', $post->post_author ); ?></a>
						</div>
						<a class="singlebody-wrapper-relatedposts-posts-post-imagelink" href="<?php the_permalink($post->ID); ?>"><img src="<?php echo get_field('post_square_image', $post->ID)['url'] ?>" class="singlebody-wrapper-relatedposts-posts-post-imagelink-image"></a>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
</div>
<?php

get_footer();

?>