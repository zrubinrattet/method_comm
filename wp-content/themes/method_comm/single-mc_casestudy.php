<?php 

get_header();

ComponentUtil::get('nav.Main');

PartialUtil::get('hero.Main');

?>
<section class="casestudyhero">
	<h1 class="casestudyhero-title"><?php echo $post->post_title; ?></h1>
	<div class="casestudyhero-subtitle"><?php the_field('case_study_short_excerpt', $post->ID); ?></div>
	<img src="<?php echo get_field('case_study_hero_image', $post->ID)['url']; ?>" class="casestudyhero-heroimage">
</section>
<div class="main">
	<?php if( !empty( $post->post_content ) || !empty( get_field('case_study_masonry_items', $post->ID, false) ) ): ?>
		<div class="main-content">
			<?php $use_masonry = get_field('case_study_use_masonry', $post->ID); ?>
			<div class="main-content-content<?php echo $use_masonry ? ' masonry' : ''; ?>">
				<?php if( $use_masonry ):
					$masonry_items = get_field('case_study_masonry_items', $post->ID, false);
					$has_mobile_quote_shortcode = false;
					foreach( $masonry_items as $item ){
						if( strpos($item['field_case_study_masonry_item'], '[mc_mobile_quotes]') !== false ){
							$has_mobile_quote_shortcode = true;
						}
					}
					?>
					<div class="main-content-content-masonry<?php echo $has_mobile_quote_shortcode ? ' hasmobilequotes' : ''; ?>">
						<?php if( !empty($quotes = get_field('case_study_quotes', $post->ID)) ): ?>
							<div class="main-content-content-masonry-quotes">
								<img src="<?php echo get_template_directory_uri() ?>/lib/img/quotemark.png" class="main-content-content-masonry-quotes-mark">
							<?php foreach( $quotes as $quote ): ?>
								<div class="main-content-content-masonry-quotes-quote">
									<div class="main-content-content-masonry-quotes-quote-text"><?php echo $quote['case_study_quote']; ?></div>
									<div class="main-content-content-masonry-quotes-quote-author"><?php echo $quote['case_study_quote_author']; ?></div>
								</div>
							<?php endforeach; ?>
							</div>
						<?php endif; ?>
						<?php foreach( $masonry_items as $item ): ?>
							<div class="main-content-content-masonry-item">
								<?php echo do_shortcode($item['field_case_study_masonry_item']); ?>
							</div>
						<?php endforeach; ?>
					</div>
				<?php else:
					add_filter('the_content', 'wpautop');
					echo apply_filters( 'the_content', do_shortcode($post->post_content) );
					remove_filter('the_content', 'wpautop');
				endif; ?>
			</div>
			<?php if( !empty($quotes = get_field('case_study_quotes', $post->ID)) && !$use_masonry ): ?>
				<div class="main-content-quotes">
					<img src="<?php echo get_template_directory_uri() ?>/lib/img/quotemark.png" class="main-content-quotes-mark">
					<?php foreach( $quotes as $quote ): ?>
						<div class="main-content-quotes-quote">
							<div class="main-content-quotes-quote-text"><?php echo $quote['case_study_quote']; ?></div>
							<div class="main-content-quotes-quote-author"><?php echo $quote['case_study_quote_author']; ?></div>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	<?php endif; ?>
	<?php
	$mb = new ModuleBuilder();

	if( $mb->has_modules() ){
		echo '<div>';
			if( !empty( $custom_css = get_field('template_custom_css', $post->ID) ) ){
				?>
				<style type="text/css">
					<?php echo $custom_css; ?>
				</style>
				<?php
			}
			$mb->render();
			if( get_field('disable_parallelogram', $post->ID) == false ){
				PartialUtil::get('bg.Parallax');
			}
		echo '</div>';
	}
	PartialUtil::get('bg.Parallax');
	?>
</div>
<?php
get_footer();
?>