<?php 
	
	class ComponentUtil extends LoaderUtil{
		public static $files = array(
			'nav.Main' => '/src/components/nav.Main.php',
			'nav.CaseStudy' => '/src/components/nav.CaseStudy.php',
			'nav.Footer' => '/src/components/nav.Footer.php',
			'grid.CaseStudy' => '/src/components/grid.CaseStudy.php',
			'grid.Social' => '/src/components/grid.Social.php',
			'grid.Team' => '/src/components/grid.Team.php',
			'module.Accordion' => '/src/components/module.Accordion.php',
			'module.Slider' => '/src/components/module.Slider.php',
			'slider.News' => '/src/components/slider.News.php',
		);
	}
	
?>