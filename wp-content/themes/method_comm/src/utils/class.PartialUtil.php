<?php

	class PartialUtil extends LoaderUtil{
		public static $files = array(
			'hero.Main' => '/src/partials/hero.Main.php',
			'bg.Parallax' => '/src/partials/bg.Parallax.php',
			'module.WYSIWYG' => '/src/partials/module.WYSIWYG.php',
			'module.ImageShowcase' => '/src/partials/module.ImageShowcase.php',
			'module.StretchToFold' => '/src/partials/module.StretchToFold.php',
			'module.BracketText' => '/src/partials/module.BracketText.php',
			'module.Banner' => '/src/partials/module.Banner.php',
			'grid.AdaptiveTwoColumn' => '/src/partials/grid.AdaptiveTwoColumn.php',
			'grid.StaggeredList' => '/src/partials/grid.StaggeredList.php',
			'grid.ImageList' => '/src/partials/grid.ImageList.php',
			'grid.ImageText' => '/src/partials/grid.ImageText.php',
			'grid.Dynamic' => '/src/partials/grid.Dynamic.php',
			'grid.HRList' => '/src/partials/grid.HRList.php',
			'grid.TextList' => '/src/partials/grid.TextList.php',
			'grid.PostHighlights' => '/src/partials/grid.PostHighlights.php',
			'grid.WindowPane' => '/src/partials/grid.WindowPane.php',
			'griditem.Post' => '/src/partials/griditem.Post.php',
			'module.ImageList' => '/src/partials/module.ImageList.php',
			'module.SinglePost' => '/src/partials/module.SinglePost.php',
		);
	}

?>