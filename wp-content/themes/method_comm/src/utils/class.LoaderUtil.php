<?php 
	/**
	 * Parent class of template element type-specific loader
	 */
	class LoaderUtil{
		/**
		 * [$files an associative array of handles and their respective file path relative to the theme directory]
		 * @var array
		 */
		public static $files;

		/**
		 * [get path to file]
		 * @param  string $name the handle of the file
		 * @param  array $loader_vars additional variables to be available at scope of include call
		 * @return null       renders loaded files
		 */
		public static function get($name, $loader_vars = array(), $post_id = null){
			
			$c = get_called_class();
			include get_template_directory() . $c::$files[$name];
		}
	}
	
?>