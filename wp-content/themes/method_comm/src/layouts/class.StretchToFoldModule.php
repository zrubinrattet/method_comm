<?php 
	
	class StretchToFoldModule extends Layout{
		protected $layout = array(
			'key' => 'field_stretchtofold_module',
			'name' => 'stretchtofold_module',
			'label' => '<strong>Stretch To Fold</strong>',
			'display' => 'block',
			'sub_fields' => array(
				array(
					'key' => 'field_stretchtofold_module_id',
					'label' => 'ID',
					'name' => 'stretchtofold_module_id',
					'type' => 'text',
					'instructions' => 'This is optional. It assigns a value to the id prop on the parent-most html tag for this module. Only use lowercase characters and numbers and hyphens. ID must start with a lowercase character.',
				),
				array(
					'key' => 'field_stretchtofold_module_text',
					'label' => 'Text',
					'name' => 'stretchtofold_module_text',
					'type' => 'textarea',
					'new_lines' => 'br',
				),
				array(
					'key' => 'field_stretchtofold_module_bg_image',
					'label' => 'Background Image',
					'name' => 'stretchtofold_module_bg_image',
					'type' => 'image',
				),
			)
		);
	}

?>