<?php 
	
	class Hero extends Layout{
		protected $layout = array(
			'key' => 'field_hero_module',
			'name' => 'hero_module',
			'label' => '<strong>Hero</strong>',
			'display' => 'block',
			'sub_fields' => array(
				array(
					'key' => 'field_hero_module_id',
					'label' => 'ID',
					'name' => 'hero_module_id',
					'type' => 'text',
					'instructions' => 'This is optional. It assigns a value to the id prop on the parent-most html tag for this module. Only use lowercase characters and numbers and hyphens. ID must start with a lowercase character.',
				),
				array(
					'key' => 'field_hero_module_size',
					'label' => 'Size',
					'name' => 'hero_module_size',
					'type' => 'radio',
					'choices' => array(
						'big' => 'Full Height',
						'small' => 'Small',
					),
				),
			)
		);
	}

?>