<?php 
	
	class TeamGrid extends Layout{
		public function __construct(){
			$this->layout = array(
				'key' => 'field_team_grid',
				'name' => 'team_grid',
				'label' => '<strong>Team Grid</strong>',
				'display' => 'block',
				'sub_fields' => array(
					array(
						'key' => 'field_team_grid_id',
						'label' => 'ID',
						'name' => 'team_grid_id',
						'type' => 'text',
						'instructions' => 'This is optional. It assigns a value to the id prop on the parent-most html tag for this module. Only use lowercase characters and numbers and hyphens. ID must start with a lowercase character.',
					),
					array(
						'key' => 'field_team_grid_title',
						'label' => 'Title',
						'name' => 'team_grid_title',
						'type' => 'text',
						'instructions' => 'This is optional. If left blank it won\'t show up.'
					),
					array(
						'key' => 'field_team_grid_users',
						'label' => 'Users',
						'name' => 'team_grid_users',
						'type' => 'user',
						'multiple' => 1,
						'ui' => 1,
					),
				)
			);
		}
	}
?>