<?php 
	
	class ImageListModule extends Layout{
		protected $layout = array(
			'key' => 'field_imagelist_module',
			'name' => 'imagelist_module',
			'label' => '<strong>Image List</strong>',
			'display' => 'block',
			'sub_fields' => array(
				array(
					'key' => 'field_imagelist_module_id',
					'label' => 'ID',
					'name' => 'imagelist_module_id',
					'type' => 'text',
					'instructions' => 'This is optional. It assigns a value to the id prop on the parent-most html tag for this module. Only use lowercase characters and numbers and hyphens. ID must start with a lowercase character.',
				),
				array(
					'key' => 'field_imagelist_module_title',
					'label' => 'Title',
					'name' => 'imagelist_module_title',
					'type' => 'text',
				),
				array(
					'key' => 'field_imagelist_module_images',
					'label' => 'Images',
					'name' => 'imagelist_module_images',
					'type' => 'gallery',
				),
			)
		);
	}

?>