<?php 
	
	class AdaptiveTwoColumnGrid extends Layout{
		protected $layout = array(
			'key' => 'field_adaptivetwocolumn_grid',
			'name' => 'adaptivetwocolumn_grid',
			'label' => '<strong>Adaptive Two Column Grid</strong>',
			'display' => 'block',
			'sub_fields' => array(
				array(
					'key' => 'field_adaptivetwocolumn_grid_id',
					'label' => 'ID',
					'name' => 'adaptivetwocolumn_grid_id',
					'type' => 'text',
					'instructions' => 'This is optional. It assigns a value to the id prop on the parent-most html tag for this module. Only use lowercase characters and numbers and hyphens. ID must start with a lowercase character.',
				),
				array(
					'key' => 'field_adaptivetwocolumn_grid_bg_color',
					'label' => 'Background Color',
					'name' => 'adaptivetwocolumn_grid_bg_color',
					'type' => 'color_picker',
				),
				array(
					'key' => 'field_adaptivetwocolumn_grid_title',
					'label' => 'Title',
					'name' => 'adaptivetwocolumn_grid_title',
					'type' => 'text',
				),
				array(
					'key' => 'field_adaptivetwocolumn_grid_items',
					'label' => 'Items',
					'name' => 'adaptivetwocolumn_grid_items',
					'type' => 'repeater',
					'button_label' => 'Add New Item',
					'layout' => 'block',
					'sub_fields' => array(
						array(
							'key' => 'field_adaptivetwocolumn_grid_item_text',
							'label' => 'Text',
							'name' => 'adaptivetwocolumn_grid_item_text',
							'type' => 'text',
						),
					),
				),
			)
		);
	}

?>