<?php 
	
	class SocialGrid extends Layout{
		public function __construct(){
			// add_filter( 'acf/prepare_field/name=social_grid_posts', array($this, 'prepare_social_grid_posts') );
			$this->layout = array(
				'key' => 'field_social_grid',
				'name' => 'social_grid',
				'label' => '<strong>Social Grid</strong>',
				'display' => 'block',
				'sub_fields' => array(
					array(
						'key' => 'field_social_grid_id',
						'label' => 'ID',
						'name' => 'social_grid_id',
						'type' => 'text',
						'instructions' => 'This is optional. It assigns a value to the id prop on the parent-most html tag for this module. Only use lowercase characters and numbers and hyphens. ID must start with a lowercase character.',
					),
					array(
						'key' => 'field_social_grid_title',
						'label' => 'Title',
						'name' => 'social_grid_title',
						'type' => 'text',
						'instructions' => 'This is optional. If left blank it won\'t show up.'
					),
					array(
						'key' => 'field_social_grid_ribbon',
						'label' => 'Use Ribbon?',
						'name' => 'social_grid_ribbon',
						'type' => 'true_false',
						'ui' => 1,
						'wrapper' => array(
							'width' => 50,
						),
					),
					array(
						'key' => 'field_social_grid_ribbon_type',
						'label' => 'Select ribbon type',
						'name' => 'social_grid_ribbon_type',
						'type' => 'radio',
						'wrapper' => array(
							'width' => 50,
						),
						'choices' => array(
							'left' => 'Left',
							'right' => 'Right'
						),
						'conditional_logic' => array(
							array(
								array(
									'field' => 'field_social_grid_ribbon',
									'operator' => '==',
									'value' => 1,
								),
							),
						),
					),
				)
			);

		}
		public function prepare_social_grid_posts($field){
			$field['choices'] = $this->get_ig_posts_for_acf_select();
			// error_log(var_export($field, true));
			return $field;
		}
		public function get_ig_posts_for_acf_select(){
			// bail on heartbeat
			if( isset($_REQUEST['action']) && $_REQUEST['action'] === 'heartbeat' ) return array();

			// get the ig feed
			$api = new Instagram\Api();
			$api->setUserName('methodcom');

			$feed = $api->getFeed();

			$return = array();

			foreach( $feed->medias as $media ){
				// use json_encode with base64_encode instead of serialize cuz seralize/unserialize doesn't work with this content
				$return[base64_encode(json_encode($media))] = '<img src="' . $media->thumbnails[0]->src . '"/><span>' . $media->caption . '</span>';
			}

			return $return;
		}
	}

?>