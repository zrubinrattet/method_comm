<?php 
	
	class ImageListGrid extends Layout{
		protected $layout = array(
			'key' => 'field_image_list_grid',
			'name' => 'image_list_grid',
			'label' => '<strong>Image List Grid</strong>',
			'display' => 'block',
			'sub_fields' => array(
				array(
					'key' => 'field_image_list_grid_id',
					'label' => 'ID',
					'name' => 'image_list_grid_id',
					'type' => 'text',
					'instructions' => 'This is optional. It assigns a value to the id prop on the parent-most html tag for this module. Only use lowercase characters and numbers and hyphens. ID must start with a lowercase character.',
				),
				array(
					'key' => 'field_image_list_grid_title',
					'label' => 'Title',
					'name' => 'image_list_grid_title',
					'type' => 'text',
					'instructions' => 'This is optional. When left blank it won\'t show up.',
				),
				array(
					'key' => 'field_image_list_grid_subtitle',
					'label' => 'Subtitle',
					'name' => 'image_list_grid_subtitle',
					'type' => 'textarea',
				),
				array(
					'key' => 'field_image_list_grid_items',
					'label' => 'List Items',
					'name' => 'image_list_grid_items',
					'type' => 'gallery',
				),
			)
		);
	}

?>