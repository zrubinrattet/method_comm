<?php 
	
	class WYSIWYG extends Layout{
		protected $layout = array(
			'key' => 'field_wysiwyg_module',
			'name' => 'wysiwyg_module',
			'label' => '<strong>WYSIWYG</strong>',
			'display' => 'block',
			'sub_fields' => array(
				array(
					'key' => 'field_wysiwyg_module_id',
					'label' => 'ID',
					'name' => 'wysiwyg_module_id',
					'type' => 'text',
					'instructions' => 'This is optional. It assigns a value to the id prop on the parent-most html tag for this module. Only use lowercase characters and numbers and hyphens. ID must start with a lowercase character.',
				),
				array(
					'key' => 'field_wysiwyg_module_title',
					'label' => 'Title',
					'name' => 'wysiwyg_module_title',
					'type' => 'text',
					'instructions' => 'This is optional. When left blank it won\'t show up.',
				),
				array(
					'key' => 'field_wysiwyg_module_content',
					'label' => 'Content',
					'name' => 'wysiwyg_module_content',
					'type' => 'wysiwyg',
				),
				array(
					'key' => 'field_wysiwyg_module_brackets',
					'label' => 'Use Brackets?',
					'name' => 'wysiwyg_module_brackets',
					'type' => 'true_false',
					'instructions' => 'If content is empty this won\'t show',
					'ui' => 1,
				),
				array(
					'key' => 'field_wysiwyg_module_ribbon',
					'label' => 'Use Ribbon?',
					'name' => 'wysiwyg_module_ribbon',
					'type' => 'true_false',
					'instructions' => 'If content is empty this won\'t show',
					'ui' => 1,
					'wrapper' => array(
						'width' => 50
					),
				),
				array(
					'key' => 'field_wysiwyg_module_ribbon_direction',
					'label' => 'Ribbon Direction',
					'name' => 'wysiwyg_module_ribbon_direction',
					'type' => 'radio',
					'choices' => array(
						'left' => 'Left',
						'right' => 'Right',
					),
					'wrapper' => array(
						'width' => 50
					),
					'conditional_logic' => array(
						array(
							array(
								'field' => 'field_wysiwyg_module_ribbon',
								'operator' => '==',
								'value' => 1,
							),
						),
					),
				),
			)
		);
	}

?>