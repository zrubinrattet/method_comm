<?php 
	
	class DynamicGrid extends Layout{
		protected $layout = array(
			'key' => 'field_dynamicgrid_module',
			'name' => 'dynamicgrid_module',
			'label' => '<strong>Dynamic Grid</strong>',
			'display' => 'block',
			'sub_fields' => array(
				array(
					'key' => 'field_dynamicgrid_module_id',
					'label' => 'ID',
					'name' => 'dynamicgrid_module_id',
					'type' => 'text',
					'instructions' => 'This is optional. It assigns a value to the id prop on the parent-most html tag for this module. Only use lowercase characters and numbers and hyphens. ID must start with a lowercase character.',
				),
				array(
					'key' => 'field_dynamicgrid_module_title',
					'label' => 'Title',
					'name' => 'dynamicgrid_module_title',
					'type' => 'text',
					'instructions' => 'This is optional. When left blank it won\'t show up.',
				),
				array(
					'key' => 'field_dynamicgrid_module_mobile_tab',
					'label' => 'Mobile',
					'name' => 'dynamicgrid_module_mobile_tab',
					'type' => 'tab',
				),
				array(
					'key' => 'field_dynamicgrid_module_mobile_max_cols',
					'label' => 'Max Columns',
					'name' => 'dynamicgrid_module_mobile_max_cols',
					'type' => 'number',
					'min' => 1,
					'max' => 2,
					'instructions' => 'Pick 1 or 2',
				),
				array(
					'key' => 'field_dynamicgrid_module_mobile_rows',
					'label' => 'Rows',
					'name' => 'dynamicgrid_module_mobile_rows',
					'type' => 'repeater',
					'layout' => 'block',
					'button_label' => 'Add New Row',
					'sub_fields' => array(
						array(
							'key' => 'field_dynamicgrid_module_mobile_cells',
							'label' => 'Cells',
							'name' => 'dynamicgrid_module_mobile_cells',
							'type' => 'repeater',
							'layout' => 'block',
							'button_label' => 'Add New Cell',
							'sub_fields' => array(
								array(
									'key' => 'field_dynamicgrid_module_mobile_cell_colspan',
									'label' => 'Column Span',
									'name' => 'dynamicgrid_module_mobile_cell_colspan',
									'type' => 'number',
									'min' => 1,
									'max' => 2,
									'wrapper' => array(
										'width' => 50,
									),
									'instructions' => 'Be cautious with column span. The total of the values of the column spans on the cells per row shouldn\'t be more than the max number of columns',
								),
								array(
									'key' => 'field_dynamicgrid_module_mobile_cell_type',
									'label' => 'Cell Type',
									'name' => 'dynamicgrid_module_mobile_cell_type',
									'type' => 'radio',
									'choices' => array(
										'image' => 'Image',
										'text' => 'Text',
									),
									'wrapper' => array(
										'width' => 50,
									),
								),
								array(
									'key' => 'field_dynamicgrid_module_mobile_cell_background_image',
									'label' => 'Background Image',
									'name' => 'dynamicgrid_module_mobile_cell_background_image',
									'type' => 'image',
									'conditional_logic' => array(
										array(
											array(
												'field' => 'field_dynamicgrid_module_mobile_cell_type',
												'operator' => '==',
												'value' => 'image',
											),
										),
									),
								),
								array(
									'key' => 'field_dynamicgrid_module_mobile_cell_header',
									'label' => 'Header',
									'name' => 'dynamicgrid_module_mobile_cell_header',
									'type' => 'text',
									'conditional_logic' => array(
										array(
											array(
												'field' => 'field_dynamicgrid_module_mobile_cell_type',
												'operator' => '==',
												'value' => 'text',
											),
										),
									),
								),
								array(
									'key' => 'field_dynamicgrid_module_mobile_cell_content',
									'label' => 'Text Content',
									'name' => 'dynamicgrid_module_mobile_cell_content',
									'type' => 'wysiwyg',
									'conditional_logic' => array(
										array(
											array(
												'field' => 'field_dynamicgrid_module_mobile_cell_type',
												'operator' => '==',
												'value' => 'text',
											),
										),
									),
								),
							),
						),
					),
				),
				array(
					'key' => 'field_dynamicgrid_module_desktop_tab',
					'label' => 'Desktop',
					'name' => 'dynamicgrid_module_desktop_tab',
					'type' => 'tab',
				),
				array(
					'key' => 'field_dynamicgrid_module_desktop_enable',
					'label' => 'Enable?',
					'name' => 'dynamicgrid_module_desktop_enable',
					'type' => 'true_false',
					'ui' => 1,
					'instructions' => 'If disabled the mobile version is used on desktop.',
				),
				array(
					'key' => 'field_dynamicgrid_module_desktop_max_cols',
					'label' => 'Max Columns',
					'name' => 'dynamicgrid_module_desktop_max_cols',
					'type' => 'number',
					'min' => 1,
					'max' => 3,
					'instructions' => 'Pick between 1 and 3',
				),
				array(
					'key' => 'field_dynamicgrid_module_desktop_rows',
					'label' => 'Rows',
					'name' => 'dynamicgrid_module_desktop_rows',
					'type' => 'repeater',
					'layout' => 'block',
					'button_label' => 'Add New Row',
					'sub_fields' => array(
						array(
							'key' => 'field_dynamicgrid_module_desktop_cells',
							'label' => 'Cells',
							'name' => 'dynamicgrid_module_desktop_cells',
							'type' => 'repeater',
							'layout' => 'block',
							'button_label' => 'Add New Cell',
							'sub_fields' => array(
								array(
									'key' => 'field_dynamicgrid_module_desktop_cell_colspan',
									'label' => 'Column Span',
									'name' => 'dynamicgrid_module_desktop_cell_colspan',
									'type' => 'number',
									'min' => 1,
									'max' => 3,
									'wrapper' => array(
										'width' => 50,
									),
									'instructions' => 'Be cautious with column span. The total of the values of the column spans on the cells per row shouldn\'t be more than the max number of columns',
								),
								array(
									'key' => 'field_dynamicgrid_module_desktop_cell_type',
									'label' => 'Cell Type',
									'name' => 'dynamicgrid_module_desktop_cell_type',
									'type' => 'radio',
									'choices' => array(
										'image' => 'Image',
										'text' => 'Text',
									),
									'wrapper' => array(
										'width' => 50,
									),
								),
								array(
									'key' => 'field_dynamicgrid_module_desktop_cell_background_image',
									'label' => 'Background Image',
									'name' => 'dynamicgrid_module_desktop_cell_background_image',
									'type' => 'image',
									'conditional_logic' => array(
										array(
											array(
												'field' => 'field_dynamicgrid_module_desktop_cell_type',
												'operator' => '==',
												'value' => 'image',
											),
										),
									),
								),
								array(
									'key' => 'field_dynamicgrid_module_desktop_cell_header',
									'label' => 'Header',
									'name' => 'dynamicgrid_module_desktop_cell_header',
									'type' => 'text',
									'conditional_logic' => array(
										array(
											array(
												'field' => 'field_dynamicgrid_module_desktop_cell_type',
												'operator' => '==',
												'value' => 'text',
											),
										),
									),
								),
								array(
									'key' => 'field_dynamicgrid_module_desktop_cell_content',
									'label' => 'Text Content',
									'name' => 'dynamicgrid_module_desktop_cell_content',
									'type' => 'wysiwyg',
									'conditional_logic' => array(
										array(
											array(
												'field' => 'field_dynamicgrid_module_desktop_cell_type',
												'operator' => '==',
												'value' => 'text',
											),
										),
									),
								),
								array(
									'key' => 'field_dynamicgrid_module_desktop_cell_two_column',
									'label' => 'Text in Two Columns?',
									'name' => 'dynamicgrid_module_desktop_cell_two_column',
									'type' => 'true_false',
									'ui' => 1,
									'conditional_logic' => array(
										array(
											array(
												'field' => 'field_dynamicgrid_module_desktop_cell_type',
												'operator' => '==',
												'value' => 'text',
											),
										),
									),
								),
							),
						),
					),
				),
			)
		);
	}

?>