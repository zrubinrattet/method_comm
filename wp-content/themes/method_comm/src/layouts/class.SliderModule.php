<?php 
	
	class SliderModule extends Layout{
		protected $layout = array(
			'key' => 'field_slider_module',
			'name' => 'slider_module',
			'label' => '<strong>Slider</strong>',
			'display' => 'block',
			'sub_fields' => array(
				array(
					'key' => 'field_slider_module_id',
					'label' => 'ID',
					'name' => 'slider_module_id',
					'type' => 'text',
					'instructions' => 'This is optional. It assigns a value to the id prop on the parent-most html tag for this module. Only use lowercase characters and numbers and hyphens. ID must start with a lowercase character.',
				),
				array(
					'key' => 'field_slider_module_title',
					'label' => 'Title',
					'name' => 'slider_module_title',
					'type' => 'text',
					'instructions' => 'This is optional. When left blank it won\'t show up.',
				),
				array(
					'key' => 'field_slider_module_subtitle',
					'label' => 'Subtitle',
					'name' => 'slider_module_subtitle',
					'type' => 'textarea',
				),
				array(
					'key' => 'field_slider_module_items',
					'label' => 'Slider Items',
					'name' => 'slider_module_items',
					'type' => 'gallery',
				),
				array(
					'key' => 'field_slider_module_slider_params',
					'label' => 'Slider Parameters',
					'name' => 'slider_module_slider_params',
					'type' => 'acf_code_field',
					'mode' => 'javascript',
					'theme' => 'cobalt',
					'instructions' => 'Parameters passed to Swiper.js which can be read up on <a target="_blank" href="https://swiperjs.com/api/#parameters">here</a><br/>
					Parameters must be a valid Javascript object which can be read up on <a target="_blank" href="https://www.w3schools.com/js/js_objects.asp#midcontentadcontainer">here</a>
					<br/><br/>Example:<br/>
					<code>
						{<br/>
						&nbsp;&nbsp;&nbsp;&nbsp;speed: 400,<br/>
						&nbsp;&nbsp;&nbsp;&nbsp;effect: \'cube\',<br/>
						&nbsp;&nbsp;&nbsp;&nbsp;autoHeight: true,<br/>
						}
					</code>
					<br/>
					<br/>
					Leave blank to use default parameters from Swiper.js.',
				),
				array(
					'key' => 'field_slider_module_brackets',
					'label' => 'Use Brackets?',
					'name' => 'slider_module_brackets',
					'type' => 'true_false',
					'ui' => 1,
				),
			)
		);
	}

?>