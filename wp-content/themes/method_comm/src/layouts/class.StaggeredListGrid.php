<?php 
	
	class StaggeredListGrid extends Layout{
		protected $layout = array(
			'key' => 'field_slg_module',
			'name' => 'slg_module',
			'label' => '<strong>Staggered List Grid</strong>',
			'display' => 'block',
			'sub_fields' => array(
				array(
					'key' => 'field_slg_module_id',
					'label' => 'ID',
					'name' => 'slg_module_id',
					'type' => 'text',
					'instructions' => 'This is optional. It assigns a value to the id prop on the parent-most html tag for this module. Only use lowercase characters and numbers and hyphens. ID must start with a lowercase character.',
				),
				array(
					'key' => 'field_slg_module_items',
					'label' => 'List Items',
					'name' => 'slg_module_items',
					'type' => 'repeater',
					'button_label' => 'Add New List Item',
					'sub_fields' => array(
						array(
							'key' => 'field_slg_module_item_title',
							'label' => 'Title',
							'name' => 'slg_module_item_title',
							'type' => 'text',
						),
						array(
							'key' => 'field_slg_module_item_subtitle',
							'label' => 'Subtitle',
							'name' => 'slg_module_item_subtitle',
							'type' => 'textarea',
						),
					),
				),
			)
		);
	}

?>