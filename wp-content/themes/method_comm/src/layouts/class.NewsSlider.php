<?php 
	
	class NewsSlider extends Layout{
		protected $layout = array(
			'key' => 'field_news_slider',
			'name' => 'news_slider',
			'label' => '<strong>News Slider</strong>',
			'display' => 'block',
			'sub_fields' => array(
				array(
					'key' => 'field_news_slider_id',
					'label' => 'ID',
					'name' => 'news_slider_id',
					'type' => 'text',
					'instructions' => 'This is optional. It assigns a value to the id prop on the parent-most html tag for this module. Only use lowercase characters and numbers and hyphens. ID must start with a lowercase character.',
				),
				array(
					'key' => 'field_news_slider_bg_color',
					'label' => 'Background Color',
					'name' => 'news_slider_bg_color',
					'type' => 'color_picker',
				),
				array(
					'key' => 'field_news_slider_title',
					'label' => 'Title',
					'name' => 'news_slider_title',
					'type' => 'text',
				),
				array(
					'key' => 'field_news_slider_items',
					'label' => 'Items',
					'name' => 'news_slider_items',
					'type' => 'repeater',
					'button_label' => 'Add New Slide',
					'layout' => 'block',
					'sub_fields' => array(
						array(
							'key' => 'field_news_slider_item_attribution',
							'label' => 'Attribution',
							'name' => 'news_slider_item_attribution',
							'type' => 'image',
						),
						array(
							'key' => 'field_news_slider_item_quote',
							'label' => 'Quote',
							'name' => 'news_slider_item_quote',
							'type' => 'textarea',
						),
						array(
							'key' => 'field_news_slider_item_image',
							'label' => 'Image',
							'name' => 'news_slider_item_image',
							'type' => 'image',
						),
						array(
							'key' => 'field_news_slider_item_url',
							'label' => 'URL',
							'name' => 'news_slider_item_url',
							'type' => 'url',
						),
					),
				),
			)
		);
	}

?>