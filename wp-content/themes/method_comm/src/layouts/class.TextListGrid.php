<?php 
	
	class TextListGrid extends Layout{
		protected $layout = array(
			'key' => 'field_text_list_grid',
			'name' => 'text_list_grid',
			'label' => '<strong>Text List Grid</strong>',
			'display' => 'block',
			'sub_fields' => array(
				array(
					'key' => 'field_text_list_grid_id',
					'label' => 'ID',
					'name' => 'text_list_grid_id',
					'type' => 'text',
					'instructions' => 'This is optional. It assigns a value to the id prop on the parent-most html tag for this module. Only use lowercase characters and numbers and hyphens. ID must start with a lowercase character.',
				),
				array(
					'key' => 'field_text_list_grid_title',
					'label' => 'Title',
					'name' => 'text_list_grid_title',
					'type' => 'text',
					'instructions' => 'This is optional. When left blank it won\'t show up.',
				),
				array(
					'key' => 'field_text_list_grid_items',
					'label' => 'Items',
					'name' => 'text_list_grid_items',
					'type' => 'repeater',
					'layout' => 'block',
					'button_label' => 'Add New Grid Item',
					'sub_fields' => array(
						array(
							'key' => 'field_text',
							'label' => 'Text',
							'name' => 'text',
							'type' => 'text',
						),
					),
				),
			)
		);
	}

?>