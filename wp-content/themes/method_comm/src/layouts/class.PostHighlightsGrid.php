<?php 
	
	class PostHighlightsGrid extends Layout{
		protected $layout = array(
			'key' => 'field_posthighlights_grid',
			'name' => 'posthighlights_grid',
			'label' => '<strong>PostHighlights Grid</strong>',
			'display' => 'block',
			'sub_fields' => array(
				array(
					'key' => 'field_posthighlights_grid_id',
					'label' => 'ID',
					'name' => 'posthighlights_grid_id',
					'type' => 'text',
					'instructions' => 'This is optional. It assigns a value to the id prop on the parent-most html tag for this module. Only use lowercase characters and numbers and hyphens. ID must start with a lowercase character.',
				),
				array(
					'key' => 'field_posthighlights_grid_title',
					'label' => 'Title',
					'name' => 'posthighlights_grid_title',
					'type' => 'text',
					'instructions' => 'This is optional. When left blank it won\'t show up.',
				),
				array(
					'key' => 'field_posthighlights_grid_items',
					'label' => 'Items',
					'name' => 'posthighlights_grid_items',
					'type' => 'repeater',
					'button_label' => 'Add New Item',
					'layout' => 'block',
					'sub_fields' => array(
						array(
							'key' => 'field_posthighlights_grid_item_bg_image',
							'label' => 'Background Image',
							'name' => 'posthighlights_grid_item_bg_image',
							'type' => 'image',
							'wrapper' => array(
								'width' => 50
							),
						),
						array(
							'key' => 'field_posthighlights_grid_item_text_bg_color',
							'label' => 'Text Background Color',
							'name' => 'posthighlights_grid_item_text_bg_color',
							'type' => 'color_picker',
							'wrapper' => array(
								'width' => 50
							),
						),
						array(
							'key' => 'field_posthighlights_grid_item_title1',
							'label' => 'Title Line 1',
							'name' => 'posthighlights_grid_item_title1',
							'type' => 'text',
							'wrapper' => array(
								'width' => 50
							),
						),
						array(
							'key' => 'field_posthighlights_grid_item_title2',
							'label' => 'Title Line 2',
							'name' => 'posthighlights_grid_item_title2',
							'type' => 'text',
							'wrapper' => array(
								'width' => 50
							),
						),
						array(
							'key' => 'field_posthighlights_grid_item_split_title_alignment',
							'label' => 'Split Alignment of Title Lines?',
							'name' => 'posthighlights_grid_item_split_title_alignment',
							'type' => 'true_false',
							'wrapper' => array(
								'width' => 50
							),
							'ui' => 1,
						),
						array(
							'key' => 'field_posthighlights_grid_item_link',
							'label' => 'Link',
							'name' => 'posthighlights_grid_item_link',
							'type' => 'link',
							'wrapper' => array(
								'width' => 50
							),
							'instructions' => 'Fields used: URL and "Open link in new tab".'
						),
						array(
							'key' => 'field_posthighlights_grid_item_text',
							'label' => 'Text',
							'name' => 'posthighlights_grid_item_text',
							'type' => 'textarea',
						),
					),
				),
			)
		);
	}

?>