<?php 
	
	class HRListGrid extends Layout{
		protected $layout = array(
			'key' => 'field_hrl_module',
			'name' => 'hrl_module',
			'label' => '<strong>Horizontal Rule List Grid</strong>',
			'display' => 'block',
			'sub_fields' => array(
				array(
					'key' => 'field_hrl_module_id',
					'label' => 'ID',
					'name' => 'hrl_module_id',
					'type' => 'text',
					'instructions' => 'This is optional. It assigns a value to the id prop on the parent-most html tag for this module. Only use lowercase characters and numbers and hyphens. ID must start with a lowercase character.',
				),
				array(
					'key' => 'field_hrl_module_title',
					'label' => 'Title',
					'name' => 'hrl_module_title',
					'type' => 'text',
					'instructions' => 'This is optional. When left blank it won\'t show up.',
				),
				array(
					'key' => 'field_hrl_module_items',
					'label' => 'Items',
					'name' => 'hrl_module_items',
					'type' => 'flexible_content',
					'button_label' => 'Add New Item',
					'layouts' => array(
						array(
							'key' => 'field_hrl_module_item_text',
							'label' => 'Text',
							'name' => 'hrl_module_item_text',
							'display' => 'block',
							'sub_fields' => array(
								array(
									'key' => 'field_hrl_module_item_text_text',
									'label' => 'Text',
									'name' => 'hrl_module_item_text_text',
									'type' => 'text',
								),
							),
						),
						array(
							'key' => 'field_hrl_module_item_hr',
							'label' => 'Horizontal Rule',
							'name' => 'hrl_module_item_hr',
							'display' => 'block',
							'sub_fields' => array(
								array(
									'key' => 'field_hrl_module_item_hr_field',
									'label' => '<hr/>',
									'name' => 'hrl_module_item_hr_field',
									'type' => 'message',
								),
							),
						),
					),
				),
			)
		);
	}

?>