<?php 
	
	class ImageShowcase extends Layout{
		protected $layout = array(
			'key' => 'field_image_showcase',
			'name' => 'image_showcase',
			'label' => '<strong>Image Showcase</strong>',
			'display' => 'block',
			'sub_fields' => array(
				array(
					'key' => 'field_image_showcase_id',
					'label' => 'ID',
					'name' => 'image_showcase_id',
					'type' => 'text',
					'instructions' => 'This is optional. It assigns a value to the id prop on the parent-most html tag for this module. Only use lowercase characters and numbers and hyphens. ID must start with a lowercase character.',
				),
				array(
					'key' => 'field_image_showcase_title',
					'label' => 'Title',
					'name' => 'image_showcase_title',
					'type' => 'text',
					'instructions' => 'This is optional. When left blank it won\'t show up.',
				),
				array(
					'key' => 'field_image_showcase_gallery',
					'label' => 'Images',
					'name' => 'image_showcase_gallery',
					'type' => 'gallery',
				),
			)
		);
	}

?>