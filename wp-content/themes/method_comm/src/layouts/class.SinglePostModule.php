<?php 
	
	class SinglePostModule extends Layout{
		protected $layout = array(
			'key' => 'field_singlepost_module',
			'name' => 'singlepost_module',
			'label' => '<strong>Single Post</strong>',
			'display' => 'block',
			'sub_fields' => array(
				array(
					'key' => 'field_singlepost_module_id',
					'label' => 'ID',
					'name' => 'singlepost_module_id',
					'type' => 'text',
					'instructions' => 'This is optional. It assigns a value to the id prop on the parent-most html tag for this module. Only use lowercase characters and numbers and hyphens. ID must start with a lowercase character.',
				),
				array(
					'key' => 'field_singlepost_module_post',
					'label' => 'Post',
					'name' => 'singlepost_module_post',
					'type' => 'post_object',
				),
			)
		);
	}

?>