<?php 
	
	class Accordion extends Layout{
		protected $layout = array(
			'key' => 'field_accordion_module',
			'name' => 'accordion_module',
			'label' => '<strong>Accordion</strong>',
			'display' => 'block',
			'sub_fields' => array(
				array(
					'key' => 'field_accordion_module_id',
					'label' => 'ID',
					'name' => 'accordion_module_id',
					'type' => 'text',
					'instructions' => 'This is optional. It assigns a value to the id prop on the parent-most html tag for this module. Only use lowercase characters and numbers and hyphens. ID must start with a lowercase character.',
				),
				array(
					'key' => 'field_accordion_module_title',
					'label' => 'Title',
					'name' => 'accordion_module_title',
					'type' => 'text',
					'instructions' => 'This is optional. When left blank it won\'t show up.',
				),
				array(
					'key' => 'field_accordion_module_items',
					'label' => 'Rows',
					'name' => 'accordion_module_rows',
					'type' => 'repeater',
					'button_label' => 'Add Row',
					'layout' => 'block',
					'sub_fields' => array(
						array(
							'key' => 'field_row_title',
							'label' => 'Title',
							'name' => 'row_title',
							'type' => 'text',
						),
						array(
							'key' => 'field_row_subtitle',
							'label' => 'Subtitle',
							'name' => 'row_subtitle',
							'type' => 'textarea',
						),
						array(
							'key' => 'field_row_content',
							'label' => 'Content',
							'name' => 'row_content',
							'type' => 'wysiwyg',
						),
					),
				),
				array(
					'key' => 'field_accordion_module_ribbon',
					'label' => 'Use Ribbon?',
					'name' => 'accordion_module_ribbon',
					'type' => 'true_false',
					'ui' => 1,
					'wrapper' => array(
						'width' => 50
					),
				),
				array(
					'key' => 'field_accordion_module_ribbon_direction',
					'label' => 'Ribbon Direction',
					'name' => 'accordion_module_ribbon_direction',
					'type' => 'radio',
					'choices' => array(
						'left' => 'Left',
						'right' => 'Right',
					),
					'wrapper' => array(
						'width' => 50
					),
					'conditional_logic' => array(
						array(
							array(
								'field' => 'field_accordion_module_ribbon',
								'operator' => '==',
								'value' => 1,
							),
						),
					),
				),
			)
		);
	}

?>