<?php 
	
	class BracketTextModule extends Layout{
		protected $layout = array(
			'key' => 'field_brackettext_module',
			'name' => 'brackettext_module',
			'label' => '<strong>Bracket Text</strong>',
			'display' => 'block',
			'sub_fields' => array(
				array(
					'key' => 'field_brackettext_module_id',
					'label' => 'ID',
					'name' => 'brackettext_module_id',
					'type' => 'text',
					'instructions' => 'This is optional. It assigns a value to the id prop on the parent-most html tag for this module. Only use lowercase characters and numbers and hyphens. ID must start with a lowercase character.',
				),
				array(
					'key' => 'field_brackettext_module_text',
					'label' => 'Text',
					'name' => 'brackettext_module_text',
					'type' => 'textarea',
					'new_lines' => 'br',
				),
			)
		);
	}

?>