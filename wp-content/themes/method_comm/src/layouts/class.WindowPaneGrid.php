<?php 
	
	class WindowPaneGrid extends Layout{
		protected $layout = array(
			'key' => 'field_windowpane_grid',
			'name' => 'windowpane_grid',
			'label' => '<strong>Windowpane Grid</strong>',
			'display' => 'block',
			'sub_fields' => array(
				array(
					'key' => 'field_windowpane_grid_id',
					'label' => 'ID',
					'name' => 'windowpane_grid_id',
					'type' => 'text',
					'instructions' => 'This is optional. It assigns a value to the id prop on the parent-most html tag for this module. Only use lowercase characters and numbers and hyphens. ID must start with a lowercase character.',
				),
				array(
					'key' => 'field_windowpane_grid_title',
					'label' => 'Title',
					'name' => 'windowpane_grid_title',
					'type' => 'text',
					'instructions' => 'This is optional. When left blank it won\'t show up.',
				),
				array(
					'key' => 'field_windowpane_grid_bg_image',
					'label' => 'Background Image',
					'name' => 'windowpane_grid_bg_image',
					'type' => 'image',
				),
				array(
					'key' => 'field_windowpane_grid_items',
					'label' => 'Items',
					'name' => 'windowpane_grid_items',
					'type' => 'repeater',
					'layout' => 'block',
					'button_label' => 'Add New Item',
					'sub_fields' => array(
						array(
							'key' => 'field_windowpane_grid_item_text',
							'label' => 'Text',
							'name' => 'windowpane_grid_item_text',
							'type' => 'text',
						),
					),
				),
			)
		);
	}

?>