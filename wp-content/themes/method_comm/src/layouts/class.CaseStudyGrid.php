<?php 
	
	class CaseStudyGrid extends Layout{
		public function __construct(){
			$this->layout = array(
				'key' => 'field_casestudy_grid',
				'name' => 'casestudy_grid',
				'label' => '<strong>Case Study Grid</strong>',
				'display' => 'block',
				'sub_fields' => array(
					array(
						'key' => 'field_casestudy_grid_id',
						'label' => 'ID',
						'name' => 'casestudy_grid_id',
						'type' => 'text',
						'instructions' => 'This is optional. It assigns a value to the id prop on the parent-most html tag for this module. Only use lowercase characters and numbers and hyphens. ID must start with a lowercase character.',
					),
					array(
						'key' => 'field_casestudy_grid_title',
						'label' => 'Title',
						'name' => 'casestudy_grid_title',
						'type' => 'text',
						'instructions' => 'This is optional. When left blank it won\'t show up.',
					),
					array(
						'key' => 'field_casestudy_grid_posts',
						'label' => 'Case Studies',
						'name' => 'casestudy_grid_posts',
						'type' => 'post_object',
						'multiple' => 1,
						'ui' => 1,
						'post_type' => array('mc_casestudy'),
					),
					array(
						'key' => 'field_casestudy_grid_type',
						'label' => 'Type',
						'name' => 'casestudy_grid_type',
						'type' => 'radio',
						'choices' => array(
							'grid' => 'Grid (uses long excerpt)',
							'aside' => 'Multiple Asides (uses short excerpt)',
						),
					),
				)
			);
		}
	}

?>