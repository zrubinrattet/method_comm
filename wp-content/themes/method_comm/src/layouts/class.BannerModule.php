<?php 
	
	class BannerModule extends Layout{
		protected $layout = array(
			'key' => 'field_banner_module',
			'name' => 'banner_module',
			'label' => '<strong>Banner Module</strong>',
			'display' => 'block',
			'sub_fields' => array(
				array(
					'key' => 'field_banner_module_id',
					'label' => 'ID',
					'name' => 'banner_module_id',
					'type' => 'text',
					'instructions' => 'This is optional. It assigns a value to the id prop on the parent-most html tag for this module. Only use lowercase characters and numbers and hyphens. ID must start with a lowercase character.',
				),
				array(
					'key' => 'field_banner_module_padding',
					'label' => 'Padding',
					'name' => 'banner_module_padding',
					'type' => 'number',
					'append' => 'px',
					'instructions' => 'The padding to be applied to the top & bottom',
					'wrapper' => array(
						'width' => 50,
					),
				),
				array(
					'key' => 'field_banner_module_bg_color',
					'label' => 'Background Color',
					'name' => 'banner_module_bg_color',
					'type' => 'color_picker',
					'wrapper' => array(
						'width' => 50,
					),
				),
				array(
					'key' => 'field_banner_module_content',
					'label' => 'Content',
					'name' => 'banner_module_content',
					'type' => 'wysiwyg',
				),
			)
		);
	}

?>