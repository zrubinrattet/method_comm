<?php 
	
	class ImageTextGrid extends Layout{
		protected $layout = array(
			'key' => 'field_image_text_grid',
			'name' => 'image_text_grid',
			'label' => '<strong>Image Text Grid</strong>',
			'display' => 'block',
			'sub_fields' => array(
				array(
					'key' => 'field_image_text_grid_id',
					'label' => 'ID',
					'name' => 'image_text_grid_id',
					'type' => 'text',
					'instructions' => 'This is optional. It assigns a value to the id prop on the parent-most html tag for this module. Only use lowercase characters and numbers and hyphens. ID must start with a lowercase character.',
				),
				array(
					'key' => 'field_image_text_grid_title',
					'label' => 'Title',
					'name' => 'image_text_grid_title',
					'type' => 'text',
					'instructions' => 'This is optional. When left blank it won\'t show up.',
				),
				array(
					'key' => 'field_image_text_grid_items',
					'label' => 'Items',
					'name' => 'image_text_grid_items',
					'type' => 'repeater',
					'layout' => 'block',
					'button_label' => 'Add New Grid Item',
					'sub_fields' => array(
						array(
							'key' => 'field_image_text_grid_item_image',
							'label' => 'Image',
							'name' => 'image_text_grid_item_image',
							'type' => 'image',
						),
						array(
							'key' => 'field_image_text_grid_item_title',
							'label' => 'Title',
							'name' => 'image_text_grid_item_title',
							'type' => 'text',
						),
						array(
							'key' => 'field_image_text_grid_item_text',
							'label' => 'Text',
							'name' => 'image_text_grid_item_text',
							'type' => 'wysiwyg',
						),
					),
				),
				array(
					'key' => 'field_image_text_grid_brackets',
					'label' => 'Use Brackets?',
					'name' => 'image_text_grid_brackets',
					'type' => 'true_false',
					'ui' => 1,
					'instructions' => 'If content is empty this won\'t show',
				),
			)
		);
	}

?>