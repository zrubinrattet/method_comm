<?php 
	
	class Layout{
		/**
		 * [$layout the layout for the module builder]
		 * @var array
		 */
		protected $layout = array();
		/**
		 * [$layouts list of all layouts]
		 * @var array
		 */
		public static $layouts = array(
			array(
				'path' => '/src/layouts/class.Accordion.php',
				'class' => 'Accordion',	
			),
			array(
				'path' => '/src/layouts/class.AdaptiveTwoColumnGrid.php',
				'class' => 'AdaptiveTwoColumnGrid',
			),
			array(
				'path' => '/src/layouts/class.BannerModule.php',
				'class' => 'BannerModule',
			),
			array(
				'path' => '/src/layouts/class.BracketTextModule.php',
				'class' => 'BracketTextModule',
			),
			array(
				'path' => '/src/layouts/class.CaseStudyGrid.php',
				'class' => 'CaseStudyGrid',	
			),
			array(
				'path' => '/src/layouts/class.DynamicGrid.php',
				'class' => 'DynamicGrid',
			),
			array(
				'path' => '/src/layouts/class.HRListGrid.php',
				'class' => 'HRListGrid',
			),
			array(
				'path' => '/src/layouts/class.ImageList.php',
				'class' => 'ImageListModule',
			),
			array(
				'path' => '/src/layouts/class.ImageListGrid.php',
				'class' => 'ImageListGrid',	
			),
			array(
				'path' => '/src/layouts/class.ImageShowcase.php',
				'class' => 'ImageShowcase',	
			),
			array(
				'path' => '/src/layouts/class.ImageTextGrid.php',
				'class' => 'ImageTextGrid',	
			),
			array(
				'path' => '/src/layouts/class.NewsSlider.php',
				'class' => 'NewsSlider',
			),
			array(
				'path' => '/src/layouts/class.PostHighlightsGrid.php',
				'class' => 'PostHighlightsGrid',
			),
			array(
				'path' => '/src/layouts/class.SinglePostModule.php',
				'class' => 'SinglePostModule',
			),
			array(
				'path' => '/src/layouts/class.SliderModule.php',
				'class' => 'SliderModule',
			),
			array(
				'path' => '/src/layouts/class.SocialGrid.php',
				'class' => 'SocialGrid',	
			),
			array(
				'path' => '/src/layouts/class.StaggeredListGrid.php',
				'class' => 'StaggeredListGrid',	
			),
			array(
				'path' => '/src/layouts/class.StretchToFoldModule.php',
				'class' => 'StretchToFoldModule',
			),
			array(
				'path' => '/src/layouts/class.TeamGrid.php',
				'class' => 'TeamGrid',	
			),
			array(
				'path' => '/src/layouts/class.TextListGrid.php',
				'class' => 'TextListGrid',	
			),
			array(
				'path' => '/src/layouts/class.WindowPaneGrid.php',
				'class' => 'WindowPaneGrid',
			),
			array(
				'path' => '/src/layouts/class.WYSIWYG.php',
				'class' => 'WYSIWYG',	
			),
		);
		/**
		 * [get_layout retrieve a layout from a layout class]
		 * @return array the layout
		 */
		public function get_layout(){
			return $this->layout;
		}
		/**
		 * [get_layouts retrieve layouts for module builder]
		 * @return array the layouts array
		 */
		final public static function get_layouts(){
			$layouts_arr = array();
			foreach( Layout::$layouts as $layout ){
				require_once(get_template_directory() . $layout['path']);
				$classname = $layout['class'];
				$class = new $classname;
				$layouts_arr[] = $class->get_layout();
			}
			return $layouts_arr;
		}
	}

?>