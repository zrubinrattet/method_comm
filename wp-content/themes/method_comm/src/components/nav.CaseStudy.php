<nav class="casestudynav">
	<div class="casestudynav-wrapper">
		<a href="<?php echo site_url(); ?>" class="casestudynav-wrapper-methodlink">
			<svg class="casestudynav-wrapper-methodlink-svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 95.89 25.75"><defs><style>.cls-1{fill:#75ad99;}.cls-2{fill:#fff;}.cls-3{fill:#807f83;}</style></defs><title>logo2</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><rect class="cls-1" width="25.75" height="25.75"/><path class="cls-2" d="M19.89,8.47H10.62v0l7,4.3-7,4.31v0h9.27v2.61H6V17.16l7.14-4.35L6,8.47V5.86h13.9Z"/><path class="cls-3" d="M41.83,17.18v-6h0L39,15.69l-2.77-4.47h0v6H34.57V8.24h1.68L39,12.83l2.79-4.59h1.68v8.94Z"/><path class="cls-3" d="M52.45,17.18H47.73V8.24h4.59l.25,1.62H49.41v2H52.2v1.61H49.41v2.11h3.28Z"/><path class="cls-3" d="M59.4,9.86v7.32H57.72V9.86H55.8L56,8.24h5.16l.18,1.62Z"/><path class="cls-3" d="M70.13,17.18V13.39H66.47v3.79H64.8V8.24h1.67v3.54h3.66V8.24h1.68v8.94Z"/><path class="cls-3" d="M80.25,17.43a4.53,4.53,0,0,1-4.59-4.72,4.59,4.59,0,1,1,9.18,0A4.53,4.53,0,0,1,80.25,17.43Zm0-7.82a2.91,2.91,0,0,0-2.92,3.1,2.93,2.93,0,1,0,5.84,0A2.91,2.91,0,0,0,80.25,9.61Z"/><path class="cls-3" d="M91.48,17.18H88.69V8.24h2.73a4.36,4.36,0,0,1,4.47,4.47A4.35,4.35,0,0,1,91.48,17.18Zm-.43-7.39h-.68v5.84h.68a2.93,2.93,0,1,0,0-5.84Z"/></g></g></svg>
		</a>
		<div class="casestudynav-wrapper-center">Our Work</div>
		<div class="casestudynav-wrapper-logocontainer">
			<img src="<?php echo get_field('case_study_logo', $post->ID)['sizes']['medium']; ?>" class="casestudynav-wrapper-logocontainer-logo">
		</div>
	</div>
</nav>