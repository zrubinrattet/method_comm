<?php
$id = MiscUtil::get_field_from_loader_vars('slider_module_id', $post_id, $loader_vars);
$title = MiscUtil::get_field_from_loader_vars('slider_module_title', $post_id, $loader_vars);
$subtitle = MiscUtil::get_field_from_loader_vars('slider_module_subtitle', $post_id, $loader_vars);
$slides = MiscUtil::get_field_from_loader_vars('slider_module_items', $post_id, $loader_vars);
$params = MiscUtil::get_field_from_loader_vars('slider_module_slider_params', $post_id, $loader_vars);
$use_brackets = MiscUtil::get_field_from_loader_vars('slider_module_brackets', $post_id, $loader_vars);
?>
<script type="text/javascript">
	try {
		// statements
		var slidermodule<?php echo $loader_vars['fc_index'] ?>params = <?php echo $params; ?>;
	}
	catch(e) {
		// statements
		console.log(e);
	}
</script>
<?php
if( !empty($slides) ):
?>
<section class="slidermodule section" data-index="<?php echo $loader_vars['fc_index']; ?>"<?php echo !empty($id) ? ' id="' . $id . '"' : ''; ?>>
	<div class="slidermodule-wrapper section-wrapper">
		<?php if( !empty($title) ): ?>
			<h2 class="slidermodule-wrapper-title section-wrapper-title"><?php echo $title; ?></h2>
		<?php endif; ?>
		<?php if( !empty($use_brackets) ): ?>
			<div class="slidermodule-wrapper-bracket slidermodule-wrapper-bracket--top"></div>
		<?php endif; ?>
		<?php if( !empty($subtitle) ): ?>
			<div class="slidermodule-wrapper-subtitle"><?php echo $subtitle; ?></div>
		<?php endif; ?>
		<div class="slidermodule-wrapper-container swiper-container">
			<div class="slidermodule-wrapper-container-wrapper swiper-wrapper">
				<?php foreach( $slides as $slide ): ?>
					<div class="slidermodule-wrapper-container-wrapper-slide swiper-slide">
						<img src="<?php echo $slide['sizes']['large'] ?>" class="slidermodule-wrapper-container-wrapper-slide-image">
					</div>
				<?php endforeach; ?>
			</div>
		</div>
		<?php if( !empty($use_brackets) ): ?>
			<div class="slidermodule-wrapper-bracket slidermodule-wrapper-bracket--bottom"></div>
		<?php endif; ?>
	</div>
</section>
<?php endif; ?>