<?php
	$id = MiscUtil::get_field_from_loader_vars('accordion_module_id', $post_id, $loader_vars);
	$rows = MiscUtil::get_field_from_loader_vars('accordion_module_rows', $post_id, $loader_vars);
	$title = MiscUtil::get_field_from_loader_vars('accordion_module_title', $post_id, $loader_vars);
	$use_ribbon = MiscUtil::get_field_from_loader_vars('accordion_module_ribbon', $post_id, $loader_vars);
	$ribbon_direction = MiscUtil::get_field_from_loader_vars('accordion_module_ribbon_direction', $post_id, $loader_vars);

	if( !empty($rows) ):
?>
<section class="accordion section"<?php echo !empty($id) ? ' id="' . $id . '"' : ''; ?>>
	<div class="accordion-wrapper section-wrapper">
		<?php if( !empty($title) ): ?>
			<h1 class="accordion-wrapper-title section-wrapper-title"><?php echo $title; ?></h1>
		<?php endif; ?>
		<?php foreach( $rows as $index => $row ): ?>
			<div class="accordion-wrapper-row">
				<h2 class="accordion-wrapper-row-title"><?php echo $row['row_title']; ?></h2>
				<div class="accordion-wrapper-row-subtitle"><?php echo $row['row_subtitle']; ?></div>
				<?php if( !empty($row['row_content']) ): ?>
					<div class="accordion-wrapper-row-togglecontainer">
						<div class="accordion-wrapper-row-togglecontainer-toggle">
							&times;
						</div>
					</div>
					<div class="accordion-wrapper-row-content">
						<div class="accordion-wrapper-row-content-angle"></div>
						<?php echo apply_filters( 'the_content', do_shortcode($row['row_content']) ); ?>
					</div>
				<?php endif; ?>
			</div>
		<?php endforeach; ?>
	</div>
</section>
<?php endif; ?>