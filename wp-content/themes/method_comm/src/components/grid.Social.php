<?php
$id = MiscUtil::get_field_from_loader_vars('social_grid_id', $post_id, $loader_vars);
// get the optional title
$title = MiscUtil::get_field_from_loader_vars('social_grid_title', $post_id, $loader_vars);
// get the ribbon type
$has_ribbon = MiscUtil::get_field_from_loader_vars('social_grid_ribbon', $post_id, $loader_vars);
$ribbon_type = MiscUtil::get_field_from_loader_vars('social_grid_ribbon_type', $post_id, $loader_vars);

// get the posts
$posts = array();



// * check for instagram auth settings saved by the WD Instagram plugin
// ? BASIC DISPLAY API --- working but INSIGHTS are NOT supported
$instagramOptions = !empty(get_option('wdi_instagram_options')) ? get_option('wdi_instagram_options') : '';
if( !empty($instagramOptions) ){

	// ? access token provided by plugin
	$token = $instagramOptions['wdi_access_token'];
	
	// ? array of fields requested from Basic Display API @link https://developers.facebook.com/docs/instagram-basic-display-api/reference/media#fields
	$igFields = implode(',',[
		'id',
		'caption',
		'media_url',
		'permalink',
		'timestamp',
		'media_type',
		'thumbnail_url'
	]);

	// ? limit of items
	$limit = 3;

	// * request latest 3 posts from user
	$request = wp_safe_remote_get( 'https://graph.instagram.com/me/media?fields='.$igFields.'&access_token='.$token.'&limit='.$limit );

	if ( !is_wp_error( $request ) ) {
		$body = wp_remote_retrieve_body( $request ); 
		$json = json_decode( $body );

		// * loop posts data into our structure
		foreach( $json->data as $mediaItem ){
			$temp = [
				'image' => $mediaItem->media_type == 'IMAGE' ? $mediaItem->media_url : $mediaItem->thumbnail_url,
				'link' => $mediaItem->permalink,
				'caption' => !empty($mediaItem->caption) ? $mediaItem->caption : '',
				'date' => date('Ymd', strtotime($mediaItem->timestamp)),
			];
			$posts[] = (object) $temp;
		}
	}
} // ? end of if we have instagram options to use

if( 
	!empty($posts) ):
?>
<section class="socialgrid section"<?php echo !empty($id) ? ' id="' . $id . '"' : ''; ?>>
	<div class="socialgrid-wrapper section-wrapper">
		<?php if( !empty($title) ): ?>
			<h1 data-aos="fade-up" class="section-wrapper-title"><?php echo $title; ?></h1>
		<?php endif; ?>
		<?php if( !empty($posts) ): ?>
			<div data-aos="fade-up" class="socialgrid-wrapper-posts">
				<?php foreach( $posts as $post ): ?>
					<a target="_blank" href="<?php echo $post->link; ?>" class="socialgrid-wrapper-posts-post" style="background-image: url('<?php echo $post->image; ?>');">
						<div class="socialgrid-wrapper-posts-post-text">
							<div class="socialgrid-wrapper-posts-post-text-body"><?php echo wp_trim_words($post->caption, 66); ?></div>
							<div class="socialgrid-wrapper-posts-post-text-header">
								<?= !empty($post->date) ? '<span class="socialgrid-wrapper-posts-post-text-header-date">'.date('m/d/Y', strtotime($post->date)).'</span>' : ''; ?>
								<?= !empty($post->comments) ? '<span class="socialgrid-wrapper-posts-post-text-header-comments">'.$post->comments.' <span>&#128172;</span></span>' : ''; ?>
								<?= !empty($post->likes) ? '<span class="socialgrid-wrapper-posts-post-text-header-likes">'.$post->likes.' <span>&#10084;</span></span>' : ''; ?>
							</div>
						</div>
						<div class="socialgrid-wrapper-posts-post-tint"></div>
					</a>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
	<?php if( $has_ribbon ): ?>
		<div data-aos="fade-<?php echo $ribbon_type == 'left' ? 'right' : 'left'; ?>" class="socialgrid-ribbon ribbon ribbon--<?php echo $ribbon_type; ?>">
			<img src="<?php echo get_template_directory_uri(); ?>/lib/img/ribbon-<?php echo $ribbon_type ?>.png">
		</div>
	<?php endif; ?>
</section>
<?php endif; ?>