<?php
$id = MiscUtil::get_field_from_loader_vars('team_grid_id', $post_id, $loader_vars);
// get the users
$users = MiscUtil::get_field_from_loader_vars('team_grid_users', $post_id, $loader_vars);
// get the title
$title = MiscUtil::get_field_from_loader_vars('team_grid_title', $post_id, $loader_vars);
?>

<section class="teamgrid section"<?php echo !empty($id) ? ' id="' . $id . '"' : ''; ?>>
	<div class="teamgrid-wrapper section-wrapper">
		<?php if( !empty($title) ): ?>
			<h1 data-aos="fade-up" class="teamgrid-wrapper-title section-wrapper-title"><?php echo $title; ?></h1>
		<?php endif; ?>
		<?php if( !empty($users) ): ?>
			<div class="teamgrid-wrapper-users">
				<?php foreach( $users as $user ): ?>
					<div data-aos="fade-in" class="teamgrid-wrapper-users-user">
						<div class="teamgrid-wrapper-users-user-closed" style="background-image: url('<?php echo get_field('user_image_1', 'user_' . $user['ID'])['sizes']['large']; ?>');"></div>
						<div class="teamgrid-wrapper-users-user-reveal" style="background-image: url('<?php echo get_field('user_image_2', 'user_' . $user['ID'])['sizes']['large']; ?>');"></div>
						<div class="teamgrid-wrapper-users-user-text">
							<h3 class="teamgrid-wrapper-users-user-text-name"><?php echo $user['display_name']; ?></h3>
							<div class="teamgrid-wrapper-users-user-text-jobtitle"><?php the_field('user_title', 'user_' . $user['ID']); ?></div>
							<div class="teamgrid-wrapper-users-user-text-description"><?php echo $user['user_description']; ?></div>
							<?php
								$twitter = get_field('user_twitter', 'user_' . $user['ID']);
								$linkedin = get_field('user_linkedin', 'user_' . $user['ID']);

								if( !empty($twitter) || !empty($linkedin) ):
							?>
								<div class="teamgrid-wrapper-users-user-text-social">
									<?php if( !empty($twitter) ): ?>
										<a href="<?php echo $twitter; ?>" class="teamgrid-wrapper-users-user-text-social-link">
											<img class="teamgrid-wrapper-users-user-text-social-link-image" src="<?php echo get_template_directory_uri() ?>/lib/img/tw.png">
										</a>
									<?php endif; ?>
									<?php if( !empty($linkedin) ): ?>
										<a href="<?php echo $linkedin; ?>" class="teamgrid-wrapper-users-user-text-social-link">
											<img class="teamgrid-wrapper-users-user-text-social-link-image" src="<?php echo get_template_directory_uri() ?>/lib/img/in.png">
										</a>
									<?php endif; ?>
								</div>
							<?php endif; ?>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
</section>