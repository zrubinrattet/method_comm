<?php
$id = MiscUtil::get_field_from_loader_vars('news_slider_id', $post_id, $loader_vars);
$title = MiscUtil::get_field_from_loader_vars('news_slider_title', $post_id, $loader_vars);
$bg_color = MiscUtil::get_field_from_loader_vars('news_slider_bg_color', $post_id, $loader_vars);
$slides = MiscUtil::get_field_from_loader_vars('news_slider_items', $post_id, $loader_vars);

if( !empty($slides) ):
?>
	<section class="newsslider newsslider<?php echo $loader_vars['fc_index']; ?> section"<?php echo !empty($id) ? ' id="' . $id . '"' : ''; echo !empty($bg_color) ? ' style="background-color: ' . $bg_color . ';"' : ''; ?>>
		<div class="newsslider-wrapper section-wrapper">
			<h2 class="newsslider-wrapper-title"><?php echo $title; ?></h2>
			<div class="newsslider-wrapper-slider swiper-container">
				<div class="newsslider-wrapper-slider-buttons">
					<div class="newsslider-wrapper-slider-buttons-button swiper-button-prev"></div>
					<div class="newsslider-wrapper-slider-buttons-button swiper-button-next"></div>
				</div>
				<div class="newsslider-wrapper-slider-wrapper swiper-wrapper">
					<?php foreach( $slides as $slide ): ?>
						<div class="newsslider-wrapper-slider-wrapper-slide swiper-slide" data-url="<?php echo $slide['news_slider_item_url']; ?>">
							<div class="newsslider-wrapper-slider-wrapper-slide-attribution">
								<img src="<?php echo $slide['news_slider_item_attribution']['url']; ?>" class="newsslider-wrapper-slider-wrapper-slide-attribution-image">
							</div>
							<div class="newsslider-wrapper-slider-wrapper-slide-imagequote">
								<div class="newsslider-wrapper-slider-wrapper-slide-imagequote-quote"><?php echo $slide['news_slider_item_quote']; ?></div>
								<div class="newsslider-wrapper-slider-wrapper-slide-imagequote-imagecontainer">
									<img src="<?php echo $slide['news_slider_item_image']['url']; ?>" class="newsslider-wrapper-slider-wrapper-slide-imagecontainer-image">
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>