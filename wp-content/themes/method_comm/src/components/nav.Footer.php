<nav class="footer">
	<div class="footer-wrapper">
		<div class="footer-wrapper-logo">
			<a href="<?php echo site_url(); ?>"><svg class="footer-wrapper-logo-svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 95.89 25.75"><g data-name="Layer 2"><g data-name="Layer 1"><path d="M0 0v25.75h25.75V0zm19.89 8.47h-9.27l7 4.3-7 4.31h9.27v2.61H6v-2.53l7.14-4.35L6 8.47V5.86h13.9zM41.83 17.18v-6L39 15.69l-2.77-4.47v6h-1.66V8.24h1.68L39 12.83l2.79-4.59h1.68v8.94zM52.45 17.18h-4.72V8.24h4.59l.25 1.62h-3.16v2h2.79v1.61h-2.79v2.11h3.28zM59.4 9.86v7.32h-1.68V9.86H55.8l.2-1.62h5.15l.19 1.62zM70.13 17.18v-3.79h-3.66v3.79H64.8V8.24h1.67v3.54h3.66V8.24h1.68v8.94zM80.25 17.43a4.53 4.53 0 01-4.59-4.72 4.59 4.59 0 119.18 0 4.53 4.53 0 01-4.59 4.72zm0-7.82a2.91 2.91 0 00-2.92 3.1 2.93 2.93 0 105.84 0 2.91 2.91 0 00-2.92-3.1zM91.48 17.18h-2.79V8.24h2.73a4.36 4.36 0 014.47 4.47 4.35 4.35 0 01-4.41 4.47zm-.43-7.39h-.68v5.84h.68a2.93 2.93 0 100-5.84z"/></g></g></svg></a>
			<div class="footer-wrapper-logo-parent"><?php the_field('footer_parent_company', 'option'); ?></div>
		</div>
		<div class="footer-wrapper-menu">
			<?php
			$nav_items = wp_get_nav_menu_items( 'footer-nav' );
			if( !empty($nav_items) ):
			?>
				<ul class="footer-wrapper-menu-pagelist">
					<?php foreach( $nav_items as $nav_item ): ?>
						<li class="footer-wrapper-menu-pagelist-item">
							<a href="<?php echo $nav_item->url; ?>" class="footer-wrapper-menu-pagelist-item-link"><?php echo $nav_item->title; ?></a>
						</li>
					<?php endforeach; ?>
				</ul>
			<?php endif; ?>
			<ul class="footer-wrapper-menu-sociallist">
				<li class="footer-wrapper-menu-sociallist-item">
					<a target="_blank" href="https://www.linkedin.com/company/method-communications" class="footer-wrapper-menu-sociallist-item-link">
						<img class="footer-wrapper-menu-sociallist-item-link-image" src="<?php echo get_template_directory_uri(); ?>/lib/img/in.png">
					</a>
				</li>
				<li class="footer-wrapper-menu-sociallist-item">
					<a target="_blank" href="https://www.facebook.com/MethodCommunications/" class="footer-wrapper-menu-sociallist-item-link">
						<img class="footer-wrapper-menu-sociallist-item-link-image" src="<?php echo get_template_directory_uri(); ?>/lib/img/fb.png">
					</a>
				</li>
				<li class="footer-wrapper-menu-sociallist-item">
					<a target="_blank" href="https://twitter.com/methodcom" class="footer-wrapper-menu-sociallist-item-link">
						<img class="footer-wrapper-menu-sociallist-item-link-image" src="<?php echo get_template_directory_uri(); ?>/lib/img/tw.png">
					</a>
				</li>
				<li class="footer-wrapper-menu-sociallist-item">
					<a target="_blank" href="https://www.instagram.com/methodcom/" class="footer-wrapper-menu-sociallist-item-link">
						<img class="footer-wrapper-menu-sociallist-item-link-image" src="<?php echo get_template_directory_uri(); ?>/lib/img/ig.png">
					</a>
				</li>
			</ul>
		</div>
		<div class="footer-wrapper-copyright">© <?php echo date('Y'); echo ' ' . get_bloginfo( 'blogname' ); ?></div>
	</div>
</nav>