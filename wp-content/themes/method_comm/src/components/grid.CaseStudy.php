<?php
$id = MiscUtil::get_field_from_loader_vars('casestudy_grid_id', $post_id, $loader_vars);
$posts = MiscUtil::get_field_from_loader_vars('casestudy_grid_posts', $post_id, $loader_vars);
$title = MiscUtil::get_field_from_loader_vars('casestudy_grid_title', $post_id, $loader_vars);
$grid_type = MiscUtil::get_field_from_loader_vars('casestudy_grid_type', $post_id, $loader_vars);

if( $grid_type == 'grid' ){
	?>
	<script type="text/javascript">
		<?php
		$js_posts = array();

		foreach( $posts as $post ){
			$temp = array();
			$temp['image'] = get_field('case_study_logo', $post->ID)['sizes']['medium'];
			$temp['excerpt'] = get_field('case_study_long_excerpt', $post->ID);
			$temp['permalink'] = get_permalink( $post->ID );
			$temp['bg'] = get_field('case_study_grid_image', $post->ID)['url'];
			$temp['title'] = get_field('case_study_grid_item_title', $post->ID);
			$js_posts[] = $temp;
		}
		?>
		var casestudygrid<?php echo $loader_vars['fc_index']; ?> = <?php echo json_encode($js_posts); ?>;
	</script>
	<?php
}
?>
<section data-scriptid="casestudygrid<?php echo $loader_vars['fc_index']; ?>" class="casestudygrid section"<?php echo !empty($id) ? ' id="' . $id . '"' : ''; ?>>
	<?php
		if( !empty($title) ):
	?>
		<h1 class="casestudygrid-title section-wrapper-title"><?php echo $title; ?></h1>
	<?php
		endif;
	?>
	<div class="casestudygrid-wrapper section-wrapper--xxlarge">
		<?php
			// we got posts right?
			if( !empty($posts) ):
				// what's the grid type?
				switch ($grid_type) {
					case 'aside':
						// loop through the posts
						foreach( $posts as $index => $post ):
							// if we're at a new aside
							$bgimg = get_field('case_study_aside_image', $post->ID);
							if( $index % 3 === 0 ):
							?>
								<div data-aos="<?php echo $index % 2 === 0 ? 'fade-right' : 'fade-left' ?>" data-aos-duration="700" class="casestudygrid-wrapper-aside">
							<?php
							endif;
							?>
									<a href="<?php echo get_permalink( $post->ID ); ?>" class="casestudygrid-wrapper-aside-post">
										<div class="casestudygrid-wrapper-aside-post-imagecontainer">
											<img class="casestudygrid-wrapper-aside-post-imagecontainer-image" src="<?php echo $bgimg['sizes']['medium']; ?>" srcset="<?php echo wp_get_attachment_image_srcset( $bgimg['id'] ); ?>">
										</div>
										<div class="casestudygrid-wrapper-aside-post-text">
											<h2 class="casestudygrid-wrapper-aside-post-text-title"><?php echo $post->post_title; ?></h2>
											<h6 class="casestudygrid-wrapper-aside-post-text-subtitle"><?php the_field('case_study_short_excerpt', $post->ID); ?></h6>
										</div>
									</a>
							<?php
							// if we're at a new aside end the wrapper
							if( $index % 3 === 2 ):
							?>
								</div>
							<?php
							endif;
						endforeach;
						break;
					
					case 'grid':
						?>
						<div class="casestudygrid-wrapper-grid">
							<?php foreach( $posts as $index => $post ): if($index > 3) continue;?>
								<div data-aos="fade-in" class="casestudygrid-wrapper-grid-item">
									<div class="casestudygrid-wrapper-grid-item-text">
										<div class="casestudygrid-wrapper-grid-item-text-logocontainer">
											<img src="<?php echo get_field('case_study_logo', $post->ID)['url']; ?>" class="casestudygrid-wrapper-grid-item-text-logocontainer-logo">
										</div>
										<div class="casestudygrid-wrapper-grid-item-text-excerpt"><?php the_field('case_study_long_excerpt', $post->ID); ?></div>
										<a href="<?php echo get_permalink( $post->ID ); ?>" class="casestudygrid-wrapper-grid-item-text-link">learn more</a>
									</div>
									<div class="casestudygrid-wrapper-grid-item-fade"></div>
									<div class="casestudygrid-wrapper-grid-item-titlecontainer">
										<div class="casestudygrid-wrapper-grid-item-titlecontainer-title"><?php the_field('case_study_grid_item_title', $post->ID); ?></div>
									</div>
									<div style="background-image: url('<?php echo get_field('case_study_grid_image', $post->ID)['url']; ?>')" class="casestudygrid-wrapper-grid-item-bgimage"></div>
								</div>
							<?php endforeach; ?>
						</div>
						<?php if(count($posts) > 3): ?>
							<div class="casestudygrid-wrapper-seemore">See more</div>
						<?php endif; ?>
						<?php
						break;
				}
			endif;
		?>
	</div>
</section>