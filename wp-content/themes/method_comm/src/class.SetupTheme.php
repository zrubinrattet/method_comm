<?php 
/**
 * Main Backend/WP Tweaking/Etc. Handler
 */
class SetupTheme{
	/**
	 * [_init entry point]
	 */
	public static function _init(){
		// basic theme setup (enqueueing scripts, adding theme supports, cleaning up the head element output, etc.)
		show_admin_bar( false );
		// prevent wysiwyg editors from creating p tags etc.
		remove_filter('the_content', 'wpautop');
		// Disable WordPress sanitization to allow more than just $allowedtags from /wp-includes/kses.php.
		remove_filter('pre_user_description', 'wp_filter_kses');
		// Add sanitization for WordPress posts.
		add_filter( 'pre_user_description', 'wp_filter_post_kses');
		update_option( 'show_avatars', false ); // cuz we're using custom stuff
		add_action( 'after_setup_theme', 'SetupTheme::after_setup_theme' );
		add_action( 'wp_enqueue_scripts', 'SetupTheme::wp_enqueue_scripts' );
		add_action( 'init', 'SetupTheme::clean_head' );
		add_filter( 'body_class', 'SetupTheme::add_slug_to_body_class' );
		add_action( 'admin_init', 'SetupTheme::hide_editor' );

		// remove customizer from the Appearance menu
		add_action( 'admin_menu', 'SetupTheme::remove_menu_clutter', 9999 );

		// tinymce editor stuff... add lato to the default font for the tinymce editor, size buttons etc.
		add_editor_style('build/css/editor-style.css');
		add_filter( 'mce_buttons_2', 'SetupTheme::scanwp_buttons' );
		add_filter( 'tiny_mce_before_init', 'SetupTheme::set_editor_font_sizes');

		// register nav menus
		register_nav_menu( 'main-nav', 'The main nav on the header' );
		register_nav_menu( 'footer-nav', 'The footer nav' );

		// custom post type stuff
		add_action( 'init', 'SetupTheme::register_custom_post_types', 5 );

		// build shortcodes
		add_shortcode( 'mc_contact_form', 'SetupTheme::build_contact_form' );
		add_shortcode( 'mc_mobile_quotes', 'SetupTheme::build_case_study_mobile_quotes' );

		// build contact form ajax handler
		add_action( 'wp_ajax_mc_contact_form_submit', 'SetupTheme::handle_contact_form' );
		add_action( 'wp_ajax_nopriv_mc_contact_form_submit', 'SetupTheme::handle_contact_form' );

		// build postarchive ajax handler
		add_action( 'wp_ajax_postarchive_seemore', 'SetupTheme::handle_postarchive_seemore' );
		add_action( 'wp_ajax_nopriv_postarchive_seemore', 'SetupTheme::handle_postarchive_seemore' );

		// remove the comments from the admin menu bar
		add_action( 'wp_before_admin_bar_render', 'SetupTheme::remove_comments_from_admin_bar' );

		// rewrite the default post type slug
		add_filter( 'register_post_type_args', function($args, $post_type){
			if( $post_type === 'post' ){
				$args['rewrite'] = array(
					'slug' => 'insight',
					'with_front' => true,
				);
				$args['has_archive'] = 'insights';
			}
			return $args;
		}, 2, 2 );
		add_filter( 'pre_post_link', function($permalink, $post){
			if ($post->post_type === 'post') {
				return '/insight/%postname%/';
			}
			return $permalink;
		}, 10, 2 );
		add_action( 'init', function(){
			remove_post_type_support('post', 'thumbnail');
			register_taxonomy('post_tag', array());
		} );
	}
	/**
	 * [html_table builds an html table from an associative array]
	 * @param  array  $data associative array of data
	 * @return string       the html table
	 */
	private static function html_table($data = array()){
		$rows = array();
		foreach ($data as $key => $val) {
			$rows[] = "<tr><td>" . $key . '</td><td>' . $val . "</td></tr>";
		}
		return "<table>" . implode('', $rows) . "</table>";
	}
	public static function remove_comments_from_admin_bar() {
		global $wp_admin_bar;
		$wp_admin_bar->remove_menu('comments');
	}
	public static function remove_menu_clutter(){
		global $submenu;
		// remove customizer
		if ( isset( $submenu[ 'themes.php' ] ) ) {
			foreach ( $submenu[ 'themes.php' ] as $index => $menu_item ) {
				if ( in_array( 'customize', $menu_item ) ) {
					unset( $submenu[ 'themes.php' ][ $index ] );
				}
			}
		}
		// remove comments
		remove_menu_page( 'edit-comments.php' );
	}
	public static function handle_postarchive_seemore(){
		if( isset($_POST['postArchive']) ){
			global $wpdb;

			$banner_post_ids = array_column($wpdb->get_results("select ID from " . $wpdb->prefix . "posts as p
														join " . $wpdb->prefix . "postmeta as pm
															on pm.post_id = p.ID
														where
															pm.meta_key = 'post_banner'
															and
															p.post_status = 'publish'
															and
															pm.meta_value = 1
														order by
															p.post_title;", ARRAY_A), 'ID');

			if( empty($banner_post_ids) ){
				$banner_post_ids = array(-1);
			}

			$non_banner_post_ids = array_column($wpdb->get_results("select distinct ID from " . $wpdb->prefix . "posts as p
														join " . $wpdb->prefix . "postmeta as pm
															on pm.post_id = p.ID
														where
															p.ID not in (" . implode(',', $banner_post_ids) . ")
															and
															p.post_status = 'publish'
															and
															p.post_type = 'post'
														order by
															p.post_date desc;", ARRAY_A), 'ID');
			if( $banner_post_ids[0] === -1 ){
				$banner_post_ids = array();
			}
			$post_ids = array_merge($banner_post_ids, $non_banner_post_ids);

			$posts = array();

			foreach( $post_ids as $post_id ){
				$posts[] = get_post($post_id);
			}

			$posts = array_slice($posts, $_POST['offset'], 10);
		}
		else{
			$args = array(
				'post_type' => 'post',
				'post_status' => 'publish',
				'posts_per_page' => 10,
				'offset' => $_POST['offset'],
			);

			if( isset($_POST['author']) ){
				$args['post_author'] = $_POST['author'];
			}

			if( isset($_POST['cat_id']) ){
				$args['tax_query'] = array(
					array(
						array(
							'taxonomy' => 'category',
							'field' => 'id',
							'terms' => $_POST['cat_id'],
						)
					),
				);
			}

			$posts = get_posts($args);
		}

		ob_start();

		foreach( $posts as $post ){
			PartialUtil::get('griditem.Post', array(
				'post' => $post,
			));
		}

		echo ob_get_clean();
		
		wp_die();
	}
	/**
	 * [handle_contact_form fired when contact form is submitted]
	 */
	public static function handle_contact_form(){
		// validate nonce
		if( !wp_verify_nonce( $_POST['nonce'], 'mc-contact-form' ) ) {
			/**
			 * status:
			 * 		0 = error
			 * 		1 = warning
			 * 		2 = success
			 */
			echo json_encode(array('status' => 0, 'message' => 'Invalid nonce 👀'));
			wp_die();
		}
		// sanitize $_POST
		$_POST = array_map('sanitize_text_field', $_POST);
		// bail if name or email are empty
		if( empty($_POST['name']) || empty($_POST['email']) ){
			// send a message
			echo json_encode(array('status' => 0, 'message' => 'Name and email are required'));
			wp_die();
		}
		// otherwise try to send an email
		else{
			// maybe store email somewhere in the db?
			$to = 'hello@methodcommunications.com';
			// set the subject
			$subject = 'New form submission';
			// remove the action & nonce keys from $_POST
			unset($_POST['action']);
			unset($_POST['nonce']);
			// build an html table from the $_POST array
			$body = SetupTheme::html_table($_POST);
			// set headers to support html email
			$headers = array('Content-Type: text/html; charset=UTF-8');
			// set the from email
			$headers[] = "From: {$_POST['name']} <{$_POST['email']}>";
			// try to send an email
			if( wp_mail( $to, $subject, $body, $headers ) ){
				// email successful
				echo json_encode(array('status' => 2, 'message' => 'Thank you for your submission!'));
			}
			else{
				// email failed
				echo json_encode(array('status' => 0, 'message' => 'Failed to send message. Check your submission data.'));
			}
			wp_die();
		}

	}
	/**
	 * [build_case_study_mobile_quotes renders the mobile quotes for the case study]
	 */
	public static function build_case_study_mobile_quotes(){
		global $post;
		ob_start();
		if( !empty($quotes = get_field('case_study_quotes', $post->ID)) ): ?>
			<div class="main-content-content-masonry-item-quotes">
				<img src="<?php echo get_template_directory_uri() ?>/lib/img/quotemark.png" class="main-content-content-masonry-item-quotes-mark">
			<?php foreach( $quotes as $quote ): ?>
				<div class="main-content-content-masonry-item-quotes-quote">
					<div class="main-content-content-masonry-item-quotes-quote-text"><?php echo $quote['case_study_quote']; ?></div>
					<div class="main-content-content-masonry-item-quotes-quote-author"><?php echo $quote['case_study_quote_author']; ?></div>
				</div>
			<?php endforeach; ?>
			</div>
		<?php endif;
		return ob_get_clean();
	}
	/**
	 * [build_contact_form renders the contact form via shortcode]
	 */
	public static function build_contact_form(){
		ob_start();
		?>
		<div class="contactform">
			<div class="contactform-bracket contactform-bracket--top"></div>
			<h2 class="contactform-header">We'd Love To Hear From You.</h2>
			<form action="" method="post" class="contactform-form" autocomplete="off">
				<input class="contactform-form-input" type="text" name="name" placeholder="Name (required)"/>
				<input class="contactform-form-input" type="email" name="email" placeholder="Email (required)"/>
				<input class="contactform-form-input" type="text" name="phone" placeholder="Phone"/>
				<textarea class="contactform-form-input" name="message" placeholder="What can we help you with?"></textarea>
				<input type="hidden" name="nonce" value="<?php echo wp_create_nonce( 'mc-contact-form' ); ?>"/>
				<input class="contactform-form-submit" type="submit" value="Send"/>
			</form>
			<div class="contactform-bracket contactform-bracket--bottom"></div>
		</div>
		<?php
		return ob_get_clean();
	}
	/**
	 * [set_editor_font_sizes adds a list of font sizes to the wysiwyg editor]
	 * @param array $settings the settings passed to tinymce
	 */
	public static function set_editor_font_sizes($settings){
		$settings['fontsize_formats'] = implode(' ', array_map(function($el){
			return $el . 'px';
		}, range(1, 120)));
		return $settings;
	}
	/**
	 * [scanwp_buttons adds the font size button the wysiwyg editor's menu]
	 * @param  array $buttons the buttons
	 * @return array          the modded buttons
	 */
	public static function scanwp_buttons( $buttons ) {
		array_unshift( $buttons, 'fontsizeselect' ); 
		return $buttons;
	}
	/**
	 * [hide_editor hides the editor when the module builder template is selected]
	 */
	public static function hide_editor() {
		// try to get the post id from the admin screen
		$post_id = @$_GET['post'] ? @$_GET['post'] : @$_POST['post_ID'];
		// bail if no post id
		if( !isset( $post_id ) ) return;
		// remove editor on method template
		if(get_page_template_slug($post_id) == 'page-default.php'){
			remove_post_type_support('page', 'editor');
			remove_post_type_support('page', 'thumbnail');
		}
	}
	/**
	 * [register_custom_post_types builds the case study post type]
	 */
	public static function register_custom_post_types(){		
		$labels = array(
			'name'                  => 'Case Studies',
			'singular_name'         => 'Case Study',
			'add_new'               => 'Add New Case Study',
			'add_new_item'          => 'Add New Case Study',
			'edit_item'             => 'Edit Case Study',
			'new_item'              => 'New Case Study',
			'view_item'             => 'View Case Study',
			'search_items'          => 'Search Case Studies',
			'not_found'             => 'No Case Studies found',
			'not_found_in_trash'    => 'No Case Studies found in Trash',
			'parent_item_colon'     => 'Parent Case Study:',
			'menu_name'             => 'Case Studies',
			'uploaded_to_this_item' => 'Uploaded to this Case Study',
			'archives'              => 'Case Study Archives',
			'attributes'            => 'Case Study Attributes',
			'insert_into_item'      => 'Insert into archive',
		);
	
		$args = array(
			'labels'              => $labels,
			'hierarchical'        => false,
			'description'         => 'description',
			'taxonomies'          => array(),
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => null,
			'menu_icon'           => 'dashicons-media-text',
			'show_in_nav_menus'   => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'has_archive'         => true,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => array(
				'slug' => 'case-study'
			),
			'capability_type'     => 'post',
			'supports'            => array(
				'title', 'editor', 'author',
				'custom-fields', 'trackbacks',
				'revisions', 'post-formats'
				)
		);
		register_post_type( 'mc_casestudy', $args );
	}
	/**
	 * [register_ajax_callbacks run to instantiate all the classes controlling ajax behaviors]
	 */
	public static function register_ajax_callbacks(){
		// loop through files in seemores dir
		foreach( glob(get_template_directory() . '/src/seemores/*.php') as $fn ){
			// get the classname
			preg_match('/class\.(.*)\.php/', $fn, $match);
			// avoid the base class and register ajax cbs
			if( ($classname = $match[1]) != 'Seemore' ) new $classname(true);
		}
	}
	/**
	 * [add_slug_to_body_class grabs the page/post's post_name and appends to the body's classlist]
	 * @param array $classes 1d array of classes
	 */
	public static function add_slug_to_body_class($classes){
		// When using https://developer.wordpress.org/reference/functions/body_class the current page slug gets added
		global $post;
		if (is_home()) {
			$key = array_search('blog', $classes);
			if ($key > -1) {
				unset($classes[$key]);
			}
		} elseif (is_page()) {
			$classes[] = sanitize_html_class($post->post_name);
		} elseif (is_singular()) {
			$classes[] = sanitize_html_class($post->post_name);
		}
		
		return $classes;
	}
	/**
	 * [after_setup_theme config extra supports]
	 */
	public static function after_setup_theme(){
		add_theme_support( 'html5' );
		add_theme_support( 'post-thumbnails' );	
	}
	/**
	 * [wp_enqueue_scripts setup scripts for front end]
	 */
	public static function wp_enqueue_scripts(){
		SetupTheme::register_styles();
		SetupTheme::enqueue_styles();
		SetupTheme::register_javascript();
		SetupTheme::enqueue_javascript();
		// expose ajax_url to the front end
		wp_localize_script( 'theme', 'ajax_url', admin_url( 'admin-ajax.php' ) );

		if( is_archive() ){
			wp_localize_script( 'theme', 'qo', (array) get_queried_object() );
		}
	}
	/**
	 * [clean_head removes bloat from head tag]
	 */
	public static function clean_head(){
		// removes generator tag
		remove_action( 'wp_head' , 'wp_generator' );
		// removes dns pre-fetch
		remove_action( 'wp_head', 'wp_resource_hints', 2 );
		// removes weblog client link
		remove_action( 'wp_head', 'rsd_link' );
		// removes windows live writer manifest link
		remove_action( 'wp_head', 'wlwmanifest_link');	
	}

	private static function enqueue_javascript(){
		wp_enqueue_script( 'theme' );
	}

	private static function enqueue_styles(){
		wp_enqueue_style( 'theme' );
	}

	private static function register_javascript(){
		wp_register_script( 'theme', get_template_directory_uri() . '/build/js/build.js' );
	}

	private static function register_styles(){
		wp_register_style( 'theme', get_template_directory_uri() . '/build/css/build.css' );
	}
}

SetupTheme::_init();

?>