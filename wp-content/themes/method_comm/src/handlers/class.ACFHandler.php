<?php
	/**
	 * Setting up acf related stuff here
	 */
	class ACFHandler {
		/**
		 * [_init entry point]
		 */
		public static function _init(){
			// register the fields
			add_action('acf/init', 'ACFHandler::register_fields');
		}
		/**
		 * [register_fields register the fields]
		 */
		public static function register_fields(){
			acf_add_local_field_group(array(
				'key' => 'group_user',
				'title' => 'User Meta',
				'fields' => array(
					array(
						'key' => 'field_user_bio_alt',
						'label' => 'Alt Bio',
						'name' => 'user_bio_alt',
						'type' => 'textarea',
						'instructions' => 'Used on post singles (illumination/post-slug)'
					),
					array(
						'key' => 'field_user_title',
						'label' => 'Job Title',
						'name' => 'user_title',
						'type' => 'text',
					),
					array(
						'key' => 'field_user_image_1',
						'label' => 'User Image 1',
						'name' => 'user_image_1',
						'type' => 'image',
						'instructions' => 'The covered image',
					),
					array(
						'key' => 'field_user_image_2',
						'label' => 'User Image 2',
						'name' => 'user_image_2',
						'type' => 'image',
						'instructions' => 'The revealed image',
					),
					array(
						'key' => 'field_user_twitter',
						'label' => 'Twitter URL',
						'name' => 'user_twitter',
						'type' => 'url',
					),
					array(
						'key' => 'field_user_linkedin',
						'label' => 'LinkedIn URL',
						'name' => 'user_linkedin',
						'type' => 'url',
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'user_role',
							'operator' => '==',
							'value' => 'all'
						),
					),
				),
			));

			acf_add_local_field_group(array(
				'key' => 'group_post',
				'title' => 'Post Meta',
				'fields' => array(
					array(
						'key' => 'field_post_archive_image',
						'label' => 'Archive Image',
						'name' => 'post_archive_image',
						'type' => 'image',
						'instructions' => 'Is used on the archive page (illuminations)',
						'wrapper' => array(
							'width' => (100 / 3),
						),
					),
					array(
						'key' => 'field_post_single_image',
						'label' => 'Single Image',
						'name' => 'post_single_image',
						'type' => 'image',
						'instructions' => 'Is used on the single page (illumination/post-slug)',
						'wrapper' => array(
							'width' => (100 / 3),
						),
					),
					array(
						'key' => 'field_post_square_image',
						'label' => 'Square Image',
						'name' => 'post_square_image',
						'type' => 'image',
						'instructions' => 'Is used on the single page (illumination/post-slug) related posts area',
						'wrapper' => array(
							'width' => (100 / 3),
						),
					),
					array(
						'key' => 'field_post_attribution',
						'label' => 'Attribution',
						'name' => 'post_attribution',
						'type' => 'wysiwyg',
						'instructions' => 'Is used on the single page (illumination/post-slug) underneath the Single Image',
					),
					array(
						'key' => 'field_post_banner',
						'label' => 'Has Banner?',
						'name' => 'post_banner',
						'type' => 'true_false',
						'ui' => 1,
					),
					array(
						'key' => 'field_post_banner_color',
						'label' => 'Banner Color',
						'name' => 'post_banner_color',
						'type' => 'color_picker',
						'wrapper' => array(
							'width' => 50
						),
						'conditional_logic' => array(
							array(
								array(
									'field' => 'field_post_banner',
									'operator' => '==',
									'value' => 1,
								),
							),
						),
					),
					array(
						'key' => 'field_post_banner_link',
						'label' => 'Banner Link/Text',
						'name' => 'post_banner_link',
						'type' => 'link',
						'instructions' => 'Fields used: URL and "Open link in new tab".',
						'wrapper' => array(
							'width' => 50
						),
						'conditional_logic' => array(
							array(
								array(
									'field' => 'field_post_banner',
									'operator' => '==',
									'value' => 1,
								),
							),
						),
					),
					array(
						'key' => 'field_post_banner_line1',
						'label' => 'Line 1',
						'name' => 'post_banner_line1',
						'type' => 'text',
						'wrapper' => array(
							'width' => 50
						),
						'conditional_logic' => array(
							array(
								array(
									'field' => 'field_post_banner',
									'operator' => '==',
									'value' => 1,
								),
							),
						),
					),
					array(
						'key' => 'field_post_banner_line2',
						'label' => 'Line 2',
						'name' => 'post_banner_line2',
						'type' => 'text',
						'wrapper' => array(
							'width' => 50
						),
						'conditional_logic' => array(
							array(
								array(
									'field' => 'field_post_banner',
									'operator' => '==',
									'value' => 1,
								),
							),
						),
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'post'
						),
					),
				),
			));

			acf_add_local_field_group(array(
				'key' => 'group_default_template',
				'title' => 'Module Builder',
				'fields' => array(
					array(
						'key' => 'field_module_buidler',
						'label' => 'Module Builder',
						'name' => 'module_builder',
						'type' => 'flexible_content',
						'layouts' => Layout::get_layouts(),
						'button_label' => 'Add Module',
					),
					array(
						'key' => 'field_template_custom_css',
						'label' => 'Custom CSS',
						'name' => 'template_custom_css',
						'type' => 'acf_code_field',
						'mode' => 'css',
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'page_template',
							'operator' => '==',
							'value' => 'page-default.php'
						),
					),
					array(
						array(
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'mc_casestudy'
						),
					),
				),
			));

			acf_add_local_field_group(array(
				'key' => 'group_pagehero',
				'title' => 'Page Meta',
				'fields' => array(
					array(
						'key' => 'field_pagehero_toggle',
						'label' => 'Use Hero Text?',
						'name' => 'pagehero_toggle',
						'type' => 'true_false',
						'ui' => 1,
					),
					array(
						'key' => 'field_pagehero_title',
						'label' => 'Title',
						'name' => 'pagehero_title',
						'type' => 'text',
						'conditional_logic' => array(
							array(
								array(
									'field' => 'field_pagehero_toggle',
									'operator' => '==',
									'value' => 1,
								),
							),
						),
					),
					array(
						'key' => 'field_pagehero_subtitle',
						'label' => 'Subtitle',
						'name' => 'pagehero_subtitle',
						'type' => 'textarea',
						'new_lines' => 'br',
						'conditional_logic' => array(
							array(
								array(
									'field' => 'field_pagehero_toggle',
									'operator' => '==',
									'value' => 1,
								),
							),
						),
					),
					array(
						'key' => 'field_disable_parallelogram',
						'label' => 'Disable Parallax Parallelogram?',
						'name' => 'disable_parallelogram',
						'type' => 'true_false',
						'ui' => 1,
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'page'
						),
						array(
							'param' => 'page',
							'operator' => '!=',
							'value' => get_option('page_on_front'),
						),
					)
				)
			));

			acf_add_local_field_group(array(
				'key' => 'group_case_study',
				'title' => 'Case Study Meta',
				'fields' => array(
					array(
						'key' => 'field_case_study_grid_item_title',
						'label' => 'Title',
						'name' => 'case_study_grid_item_title',
						'type' => 'text',
						'instructions' => 'Used in the casestudy grid of grid type "grid" as the title of the grid item',
					),
					array(
						'key' => 'field_case_study_short_excerpt',
						'label' => 'Short Excerpt',
						'name' => 'case_study_short_excerpt',
						'type' => 'text',
						'instructions' => 'Used in the casestudy grid as the subtitle in the aside layout and on the case study single as the subtitle',
					),
					array(
						'key' => 'field_case_study_long_excerpt',
						'label' => 'Long Excerpt',
						'name' => 'case_study_long_excerpt',
						'type' => 'textarea',
						'instructions' => 'Used in the casestudy grid as the excerpt in the grid layout',
					),
					array(
						'key' => 'field_case_study_aside_image',
						'label' => 'Aside Image',
						'name' => 'case_study_aside_image',
						'type' => 'image',
						'instructions' => 'Used in the aside case study type',
					),
					array(
						'key' => 'field_case_study_grid_image',
						'label' => 'Grid Image',
						'name' => 'case_study_grid_image',
						'type' => 'image',
						'instructions' => 'Used in the grid case study type',
					),
					array(
						'key' => 'field_case_study_logo',
						'label' => 'Logo',
						'name' => 'case_study_logo',
						'type' => 'image',
						'instructions' => 'Used in the casestudy grid as the logo in the grid layout and the case study single in the top right',
					),
					array(
						'key' => 'field_case_study_hero_image',
						'label' => 'Hero Image',
						'name' => 'case_study_hero_image',
						'type' => 'image',
						'instructions' => 'Used to on the case study single',
					),
					array(
						'key' => 'field_case_study_quotes',
						'label' => 'Quotes',
						'name' => 'case_study_quotes',
						'type' => 'repeater',
						'instructions' => 'Used to on the case study single',
						'button_label' => 'Add New Quote',
						'sub_fields' => array(
							array(
								'key' => 'field_case_study_quote',
								'label' => 'Quote',
								'name' => 'case_study_quote',
								'type' => 'textarea',
							),
							array(
								'key' => 'field_case_study_quote_author',
								'label' => 'Who said the quote?',
								'name' => 'case_study_quote_author',
								'type' => 'text',
							),
						),
					),
					array(
						'key' => 'field_case_study_use_masonry',
						'label' => 'Use Masonry Grid?',
						'name' => 'case_study_use_masonry',
						'type' => 'true_false',
						'ui' => 1,
					),
					array(
						'key' => 'field_case_study_masonry_items',
						'label' => 'Masonry Grid Items',
						'name' => 'case_study_masonry_items',
						'type' => 'repeater',
						'button_label' => 'Add New Masonry Grid Item',
						'instructions' => 'Make each block of text or image it\'s own grid item',
						'sub_fields' => array(
							array(
								'key' => 'field_case_study_masonry_item',
								'label' => 'Masonry Item',
								'name' => 'case_study_masonry_item',
								'type' => 'wysiwyg',
							),
						),
						'conditional_logic' => array(
							array(
								array(
									'field' => 'field_case_study_use_masonry',
									'operator' => '==',
									'value' => 1,
								),
							),
						),
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'mc_casestudy'
						),
					),
				),
			));

			acf_add_options_page(array(
				'page_title' => 'Site Settings',
				'menu_title' => 'Site Settings',
				'menu_slug' => 'site-settings',
				'capability' => 'edit_posts',
				'redirect' => false
			));

			acf_add_local_field_group(array(
				'key' => 'group_site_settings',
				'title' => 'Site Settings',
				'fields' => array(
					array(
						'key' => 'field_footer_parent_company',
						'label' => 'Parent Company Text',
						'name' => 'footer_parent_company',
						'type' => 'text',
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'options_page',
							'operator' => '==',
							'value' => 'site-settings'
						),
					),
				),
			));
		}
		public static function get_fa_icons_for_acf_select(){
			return [];
		}
	}

	ACFHandler::_init();
?>