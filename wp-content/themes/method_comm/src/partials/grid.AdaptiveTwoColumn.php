<?php
$id = MiscUtil::get_field_from_loader_vars('adaptivetwocolumn_grid_id', $post_id, $loader_vars);
$title = MiscUtil::get_field_from_loader_vars('adaptivetwocolumn_grid_title', $post_id, $loader_vars);
$items = MiscUtil::get_field_from_loader_vars('adaptivetwocolumn_grid_items', $post_id, $loader_vars);
$bg_color = MiscUtil::get_field_from_loader_vars('adaptivetwocolumn_grid_bg_color', $post_id, $loader_vars);
?>
<section class="atcg section"<?php echo !empty($id) ? ' id="' . $id . '"' : ''; echo !empty($bg_color) ? ' style="background-color: ' . $bg_color . ';"' : ''; ?>>
	<div class="atcg-wrapper section-wrapper">
		<?php if( !empty($title) ): ?>
			<h2 class="atcg-wrapper-title"><?php echo $title; ?></h2>
		<?php endif; ?>
		<?php if( !empty($items) ): ?>
			<div class="atcg-wrapper-items">
				<?php foreach( $items as $item ): ?>
					<div class="atcg-wrapper-items-item"><?php echo $item['adaptivetwocolumn_grid_item_text']; ?></div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
</section>