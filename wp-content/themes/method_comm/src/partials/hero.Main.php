<?php
global $post;
$post_id = $post->ID;
?>
<section class="hero">
	<?php
		switch ( $post_id ) {
			case get_option('page_on_front'):
				?>
				<div class="hero-big">
					<div class="hero-big-mobiletext">
						<h1 class="hero-big-mobiletext-header">Be Illuminated</h1>
						<div class="hero-big-mobiletext-subheaders">
							<h2 class="hero-big-mobiletext-subheaders-subheader">The brightest PR & marketing strategy.</h2>
							<h2 class="hero-big-mobiletext-subheaders-subheader">Results that capture the spotlight.</h2>
						</div>
					</div>
					<div class="hero-big-tint"></div>
					<div class="hero-big-videocontainer">
						<video class="hero-big-videocontainer-video" autoplay playsinline loop muted>
							<source src="<?php echo get_template_directory_uri() ?>/lib/vid/hero.mp4" type="video/mp4">
							<source src="<?php echo get_template_directory_uri() ?>/lib/vid/hero.ogv" type="video/ogg">
							<source src="<?php echo get_template_directory_uri() ?>/lib/vid/hero.webm" type="video/webm">
						</video>
					</div>
				</div>
				<?php
				break;
			
			default:
				$has_text = get_field('pagehero_toggle', $post_id);
				?>
				<div class="hero-small<?php echo $has_text ? ' hero-small--hastext' : ''; ?>">
					<?php if( $has_text ): ?>
						<div class="hero-small-text">
							<?php if( !empty( $title = get_field('pagehero_title', $post_id) ) ): ?>
								<h1 class="hero-small-text-title"><?php echo $title; ?></h1>
							<?php endif; ?>
							<?php if( !empty( $subtitle = get_field('pagehero_subtitle', $post_id) ) ): ?>
								<div class="hero-small-text-subtitle"><?php echo $subtitle; ?></div>
							<?php endif; ?>
						</div>
					<?php endif; ?>
					<video class="hero-small-videocontainer-video" autoplay playsinline loop muted>
						<source src="<?php echo get_template_directory_uri() ?>/lib/vid/hero-small.mp4" type="video/mp4">
						<source src="<?php echo get_template_directory_uri() ?>/lib/vid/hero-small.ogv" type="video/ogg">
						<source src="<?php echo get_template_directory_uri() ?>/lib/vid/hero-small.webm" type="video/webm">
					</video>
				</div>
				<?php
				break;
		}
	?>
</section>