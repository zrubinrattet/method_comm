<?php
$id = MiscUtil::get_field_from_loader_vars('singlepost_module_id', $post_id, $loader_vars);
$post = MiscUtil::get_field_from_loader_vars('singlepost_module_post', $post_id, $loader_vars);

if( !empty($post) ):
?>
	<section class="singlepostmodule section"<?php echo !empty($id) ? ' id="' . $id . '"' : ''; ?>>
		<ul class="singlepostmodule-wrapper section-wrapper">
			<?php
			PartialUtil::get('griditem.Post', array(
				'post' => $post,
				'no_dividers' => true,
			));
			?>
		</ul>
	</section>
<?php endif; ?>