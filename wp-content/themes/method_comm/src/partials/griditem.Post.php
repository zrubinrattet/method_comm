<?php
$post = $loader_vars['post'];
$no_dividers = $loader_vars['no_dividers'] ?? false;
?>
<li class="postarchive-wrapper-grid-item<?php echo $no_dividers ? ' nodividers' : ''; ?>" data-aos="fade-in" data-id="<?php echo $post->ID ?>">
	<?php if( get_field('post_banner', $post->ID) ): $link = get_field('post_banner_link', $post->ID); ?>
		<a href="<?php echo $link['url'] ?>" <?php echo !empty($link['target']) ? ' target="_blank"' : ''; ?> class="postarchive-wrapper-grid-item-banner" style="background-color: <?php the_field('post_banner_color', $post->ID); ?>">
			<div class="postarchive-wrapper-grid-item-banner-text">
				<span class="postarchive-wrapper-grid-item-banner-text-line1"><?php the_field('post_banner_line1', $post->ID); ?></span>
				<span class="postarchive-wrapper-grid-item-banner-text-line2"><?php the_field('post_banner_line2', $post->ID); ?></span>
			</div>
		</a>
	<?php endif; ?>
	<a class="postarchive-wrapper-grid-item-imagelink" href="<?php the_permalink($post->ID); ?>">
		<img src="<?php echo get_field('post_archive_image', $post->ID)['url'] ?>" class="postarchive-wrapper-grid-item-imagelink-image">
	</a>
	<div class="postarchive-wrapper-grid-item-text">
		<a class="postarchive-wrapper-grid-item-text-title<?php echo get_field('post_banner', $post->ID) ? ' hasbanner' : ''; ?>" href="<?php the_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a>
		<div class="postarchive-wrapper-grid-item-text-excerpt"><?php
			if( !empty($post->post_excerpt) ){
				echo $post->post_excerpt;
			}
			else{
				echo wp_trim_words( $post->post_content, 25 );
			}
		?></div>
		<div class="postarchive-wrapper-grid-item-text-authordate">
			<a href="<?php echo get_author_posts_url( $post->post_author ); ?>" class="postarchive-wrapper-grid-item-text-authordate-author">
				<?php echo get_the_author_meta( 'display_name', $post->post_author ); ?>
			</a>
			<div class="postarchive-wrapper-grid-item-text-authordate-date">
				<?php echo date('F j, Y', strtotime($post->post_date)); ?>
			</div>
		</div>
	</div>
</li>