<?php
$id = MiscUtil::get_field_from_loader_vars('hrl_module_id', $post_id, $loader_vars);
$title = MiscUtil::get_field_from_loader_vars('hrl_module_title', $post_id, $loader_vars);
$items = MiscUtil::get_field_from_loader_vars('hrl_module_items', $post_id, $loader_vars);

if( !empty($items) ):
?>
	<section class="hrl section">
		<div class="hrl-wrapper section-wrapper">
			<?php if( !empty($title) ): ?>
				<h1 class="hrl-wrapper-title section-wrapper-title"><?php echo $title; ?></h1>
			<?php endif; ?>
			<?php foreach( $items as $item ):
				switch ($item['acf_fc_layout']) {
					case 'hrl_module_item_text':
						?><span class="hrl-wrapper-item"><?php echo $item['hrl_module_item_text_text']; ?></span><?php
						break;
					
					case 'hrl_module_item_hr':
						?><hr><?php
						break;
				}
				?>
				
			<?php endforeach; ?>
		</div>
	</section>
<?php endif; ?>