<?php
$id = MiscUtil::get_field_from_loader_vars('imagelist_module_id', $post_id, $loader_vars);
$title = MiscUtil::get_field_from_loader_vars('imagelist_module_title', $post_id, $loader_vars);
$images = MiscUtil::get_field_from_loader_vars('imagelist_module_images', $post_id, $loader_vars);
?>
<script type="text/javascript">
	var imagelistmodule<?php echo $loader_vars['fc_index']; ?> = <?php echo json_encode($images); ?>;
</script>
<section class="imagelistmodule section"<?php echo !empty($id) ? ' id="' . $id . '"' : ''; ?> data-fc_index="<?php echo $loader_vars['fc_index']; ?>">
	<div class="imagelistmodule-wrapper section-wrapper">
		<?php if( !empty($title) ): ?>
			<h2 class="imagelistmodule-wrapper-title"><?php echo $title; ?></h2>
		<?php endif; ?>
		<?php if( !empty( $images ) ): ?>
			<div class="imagelistmodule-wrapper-images">
				
			</div>
			<?php if( count($images) > 2 ): ?>
				<a href="#" class="imagelistmodule-wrapper-seemore">See More</a>
			<?php endif; ?>
		<?php endif; ?>
	</div>
</section>