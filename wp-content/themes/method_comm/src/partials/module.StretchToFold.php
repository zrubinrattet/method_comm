<?php
$id = MiscUtil::get_field_from_loader_vars('stretchtofold_module_id', $post_id, $loader_vars);
$text = MiscUtil::get_field_from_loader_vars('stretchtofold_module_text', $post_id, $loader_vars);
$bg_image = MiscUtil::get_field_from_loader_vars('stretchtofold_module_bg_image', $post_id, $loader_vars);
?>
<section class="stretchtofold section"<?php echo !empty($id) ? ' id="' . $id . '"' : ''; ?>>
	<div class="stretchtofold-wrapper section-wrapper">
		<?php if( !empty($text) ): ?>
			<div class="stretchtofold-wrapper-text"><?php echo $text; ?></div>
		<?php endif; ?>
	</div>
	<?php if( !empty($bg_image) ): ?>
		<img src="<?php echo $bg_image['url'] ?>" class="stretchtofold-bgimage">
	<?php endif; ?>
</section>