<?php
$id = MiscUtil::get_field_from_loader_vars('dynamicgrid_module_id', $post_id, $loader_vars);
$title = MiscUtil::get_field_from_loader_vars('dynamicgrid_module_title', $post_id, $loader_vars);
$rows = array();
$rows['mobile'] = MiscUtil::get_field_from_loader_vars('dynamicgrid_module_mobile_rows', $post_id, $loader_vars);
$rows['desktop'] = MiscUtil::get_field_from_loader_vars('dynamicgrid_module_desktop_rows', $post_id, $loader_vars);
?>
<section class="dynamicgrid section"<?php echo !empty($id) ? ' id="' . $id . '"' : ''; ?>>
	<div class="dynamicgrid-wrapper section-wrapper">
		<?php if( !empty($title) ): ?>
			<h1 data-aos="fade-in" class="dynamicgrid-wrapper-text section-wrapper-title"><?php echo $title; ?></h1>
		<?php endif; ?>
		<?php
		// loop through the breakpoints
		foreach( array('mobile', 'desktop') as $breakpoint ):
			// don't render desktop view if it's disabled
			if( $breakpoint == 'desktop' && !MiscUtil::get_field_from_loader_vars('dynamicgrid_module_desktop_enable', $post_id, $loader_vars) ){
				break;
			}
			// if there's rows at that breakpoint
			if( !empty( $rows[$breakpoint] ) ): ?>
				<div class="dynamicgrid-wrapper-<?php echo $breakpoint ?>rows <?php echo 'maxcols-' . strval(MiscUtil::get_field_from_loader_vars('dynamicgrid_module_' . $breakpoint . '_max_cols', $post_id, $loader_vars)); ?>">
					<?php foreach( $rows[$breakpoint] as $row ): ?>
						<div class="dynamicgrid-wrapper-<?php echo $breakpoint ?>rows-row" data-aos="fade-in">
							<?php foreach( $row['dynamicgrid_module_' . $breakpoint . '_cells'] as $cell ): ?>
								<div class="dynamicgrid-wrapper-<?php echo $breakpoint ?>rows-row-cell <?php echo $cell_type = $cell['dynamicgrid_module_' . $breakpoint . '_cell_type']; echo ' colspan-' . strval($cell['dynamicgrid_module_' . $breakpoint . '_cell_colspan']); ?>">
									<?php
									switch( $cell_type ):
										case 'image':
											?>
											<img class="dynamicgrid-wrapper-<?php echo $breakpoint ?>rows-row-cell-image" src="<?php echo $cell['dynamicgrid_module_' . $breakpoint . '_cell_background_image']['url']; ?>">
											<?php
											break;
										case 'text':
											?>
											<div class="dynamicgrid-wrapper-<?php echo $breakpoint ?>rows-row-cell-text">
												<h3 class="dynamicgrid-wrapper-<?php echo $breakpoint ?>rows-row-cell-text-header"><?php echo $cell['dynamicgrid_module_' . $breakpoint . '_cell_header']; ?></h3>
												<div class="dynamicgrid-wrapper-<?php echo $breakpoint ?>rows-row-cell-text-content <?php echo $breakpoint === 'desktop' && $cell['dynamicgrid_module_' . $breakpoint . '_cell_two_column'] ? ' twocol' : ''; ?>"><?php echo $cell['dynamicgrid_module_' . $breakpoint . '_cell_content']; ?></div>
											</div>
											<?php
											break;
									endswitch;
									?>
								</div>
							<?php endforeach; ?>
						</div>
					<?php endforeach; ?>
				</div>
			<?php
			endif;
		endforeach;
		?>
	</div>
</section>