<?php
$id = MiscUtil::get_field_from_loader_vars('image_text_grid_id', $post_id, $loader_vars);
$title = MiscUtil::get_field_from_loader_vars('image_text_grid_title', $post_id, $loader_vars);
$items = MiscUtil::get_field_from_loader_vars('image_text_grid_items', $post_id, $loader_vars);
$use_brackets = MiscUtil::get_field_from_loader_vars('image_text_grid_brackets', $post_id, $loader_vars);

if( !empty($items) ): ?>
<section class="imagetextgrid section">
	<div class="imagetextgrid-wrapper section-wrapper">
		<?php if( !empty($title) ): ?>
			<h1 data-aos="fade-in" class="imagetextgrid-wrapper-text section-wrapper-title"><?php echo $title; ?></h1>
		<?php endif; ?>
		<div class="imagetextgrid-wrapper-items">
			<?php if( $use_brackets ): ?>
				<div data-aos="fade-right" class="imagetextgrid-wrapper-items-bracket imagetextgrid-wrapper-items-bracket--left"></div>
			<?php endif; ?>
			<div class="imagetextgrid-wrapper-items-wrapper">
				<?php foreach( $items as $index => $item ): ?>
					<div class="imagetextgrid-wrapper-items-wrapper-item" data-aos="fade-<?php echo $index % 2 == 0 ? 'right' : 'left'; ?>">
						<div class="imagetextgrid-wrapper-items-wrapper-item-image" style="background-image: url('<?php echo $item['image_text_grid_item_image']['sizes']['large']; ?>');"></div>
						<div class="imagetextgrid-wrapper-items-wrapper-item-text">
							<div class="imagetextgrid-wrapper-items-wrapper-item-text-inner">
								<h2 class="imagetextgrid-wrapper-items-wrapper-item-text-inner-title"><?php echo $item['image_text_grid_item_title']; ?></h2>
								<br/>
								<div class="imagetextgrid-wrapper-items-wrapper-item-text-inner-info"><?php echo apply_filters('the_content', $item['image_text_grid_item_text']); ?></div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
			<?php if( $use_brackets ): ?>
				<div data-aos="fade-left" class="imagetextgrid-wrapper-items-bracket imagetextgrid-wrapper-items-bracket--right"></div>
			<?php endif; ?>
		</div>
	</div>
</section>
<?php endif; ?>