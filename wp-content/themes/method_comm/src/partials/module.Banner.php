<?php
$id = MiscUtil::get_field_from_loader_vars('banner_module_id', $post_id, $loader_vars);
$bg_color = MiscUtil::get_field_from_loader_vars('banner_module_bg_color', $post_id, $loader_vars);
$padding = MiscUtil::get_field_from_loader_vars('banner_module_padding', $post_id, $loader_vars);
$content = MiscUtil::get_field_from_loader_vars('banner_module_content', $post_id, $loader_vars, false);
error_log(var_export($content, true));
$styles = !empty($bg_color) ? 'background-color: ' . $bg_color . ';' : '';
$styles .= !empty($padding) ? 'padding: ' . $padding . 'px 0px;' : '';

?>
<section class="bannermodule section" <?php echo !empty($styles) ? 'style="' . $styles . '"' : ''; ?>>
	<div class="bannermodule-wrapper section-wrapper">
		<div class="bannermodule-wrapper-content"><?php echo do_shortcode( $content ); ?></div>
	</div>
</section>