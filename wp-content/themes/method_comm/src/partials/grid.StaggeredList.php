<?php
	$id = MiscUtil::get_field_from_loader_vars('slg_module_id', $post_id, $loader_vars);
	$list_items = MiscUtil::get_field_from_loader_vars('slg_module_items', $post_id, $loader_vars);

	if( !empty($list_items) ):
?>
<section class="slg section"<?php echo !empty($id) ? ' id="' . $id . '"' : ''; ?>>
	<div class="slg-wrapper section-wrapper">
		<?php foreach( $list_items as $index => $list_item ): ?>
			<div class="slg-wrapper-item">
				<div class="slg-wrapper-item-text">
					<h1 data-aos="fade-<?php echo $index % 2 == 0 ? 'right' : 'left'; ?>" class="slg-wrapper-item-text-title"><?php echo $list_item['slg_module_item_title']; ?></h1>
					<div data-aos="fade-<?php echo $index % 2 == 0 ? 'right' : 'left'; ?>" class="slg-wrapper-item-text-subtitle"><?php echo $list_item['slg_module_item_subtitle']; ?></div>
				</div>
				<div data-aos="fade-in" class="slg-wrapper-item-number">
					<div class="slg-wrapper-item-number-container">
						<span data-aos="move-left--keepskew" class="slg-wrapper-item-number-container-number">
							<?php echo $index < 9 ? '0' . ($index + 1) : $index; ?>	
						</span>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</section>
<?php endif; ?>