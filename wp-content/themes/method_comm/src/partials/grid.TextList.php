<?php
$id = MiscUtil::get_field_from_loader_vars('text_list_grid_id', $post_id, $loader_vars);
$title = MiscUtil::get_field_from_loader_vars('text_list_grid_title', $post_id, $loader_vars);
$items = MiscUtil::get_field_from_loader_vars('text_list_grid_items', $post_id, $loader_vars);

if( !empty($items) ):
?>
<section class="tlg section">
	<div class="tlg-wrapper section-wrapper">
		<?php if( !empty($title) ): ?>
			<h1 class="tlg-wrapper-title section-wrapper-title"><?php echo $title; ?></h1>
		<?php endif; ?>
		<div class="tlg-wrapper-items">
			<?php foreach( $items as $item ): ?>
				<div class="tlg-wrapper-items-item">
					<div class="tlg-wrapper-items-item-text"><?php echo $item['text']; ?></div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>
<?php endif; ?>