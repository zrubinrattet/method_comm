<?php
	$id = MiscUtil::get_field_from_loader_vars('image_list_grid_id', $post_id, $loader_vars);
	$list_items = MiscUtil::get_field_from_loader_vars('image_list_grid_items', $post_id, $loader_vars);
	$title = MiscUtil::get_field_from_loader_vars('image_list_grid_title', $post_id, $loader_vars);
	$subtitle = MiscUtil::get_field_from_loader_vars('image_list_grid_subtitle', $post_id, $loader_vars);

	if( !empty($list_items) ):
?>
<section class="ilg section"<?php echo !empty($id) ? ' id="' . $id . '"' : ''; ?>>
	<div class="ilg-wrapper section-wrapper">
		<?php if( !empty($title) ): ?>
			<h1 data-aos="fade-in" class="ilg-wrapper-title section-wrapper-title"><?php echo $title; ?></h1>
		<?php endif; ?>
		<?php if( !empty($subtitle) ): ?>
			<div data-aos="fade-in" class="ilg-wrapper-subtitle"><?php echo $subtitle; ?></div>
		<?php endif; ?>
		<div class="ilg-wrapper-items">
			<?php foreach( $list_items as $index => $list_item ): ?>
				<div data-aos="fade-in" class="ilg-wrapper-items-item">
					<img src="<?php echo $list_item['sizes']['medium']; ?>" class="ilg-wrapper-items-item-img">
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>
<?php endif; ?>