<?php
$id = MiscUtil::get_field_from_loader_vars('windowpane_grid_id', $post_id, $loader_vars);
$title = MiscUtil::get_field_from_loader_vars('windowpane_grid_title', $post_id, $loader_vars);
$bg_image = MiscUtil::get_field_from_loader_vars('windowpane_grid_bg_image', $post_id, $loader_vars);
$items = MiscUtil::get_field_from_loader_vars('windowpane_grid_items', $post_id, $loader_vars);
?>
<section class="windowpanegrid section"<?php echo !empty($id) ? ' id="' . $id . '"' : ''; ?>>
	<div data-aos="fade-in" data-aos-offset="-200" class="windowpanegrid-wrapper section-wrapper">
		<?php if( !empty($title) ): ?>
			<h1 class="windowpanegrid-wrapper-title section-wrapper-title"><?php echo $title; ?></h1>
		<?php endif; ?>
		<?php if( !empty($items) ): ?>
			<div class="windowpanegrid-wrapper-items" style="background-image: url('<?php echo $bg_image['url']; ?>');">
				<?php foreach( $items as $item ): ?>
					<div class="windowpanegrid-wrapper-items-item">
						<div class="windowpanegrid-wrapper-items-item-text">
							<?php echo $item['windowpane_grid_item_text']; ?>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
</section>