<?php
$id = MiscUtil::get_field_from_loader_vars('wysiwyg_module_id', $post_id, $loader_vars);
$has_ribbon = MiscUtil::get_field_from_loader_vars('wysiwyg_module_ribbon', $post_id, $loader_vars);
$ribbon_direction = MiscUtil::get_field_from_loader_vars('wysiwyg_module_ribbon_direction', $post_id, $loader_vars);
$content = MiscUtil::get_field_from_loader_vars('wysiwyg_module_content', $post_id, $loader_vars, false);
$title = MiscUtil::get_field_from_loader_vars('wysiwyg_module_title', $post_id, $loader_vars);
?>
<section class="wysiwyg section"<?php echo !empty($id) ? ' id="' . $id . '"' : ''; ?>>
	<?php if(!empty($content) && $has_ribbon ): ?>
		<div class="wysiwyg-ribbon ribbon ribbon--<?php echo $ribbon_direction ?>">
			<img data-aos="fade-<?php echo $ribbon_direction == 'left' ? 'right' : 'left'; ?>" src="<?php echo get_template_directory_uri(); ?>/lib/img/ribbon-<?php echo $ribbon_direction ?>.png">
		</div>
	<?php endif; ?>
	<div data-aos="fade-in" data-aos-offset="-200" class="wysiwyg-wrapper section-wrapper">
		<?php if( !empty($title) ): ?>
			<h1 class="wysiwyg-wrapper-title section-wrapper-title"><?php echo $title; ?></h1>
		<?php endif; ?>
		<?php if( !empty($content) ): ?>
			<?php if( MiscUtil::get_field_from_loader_vars('wysiwyg_module_brackets', $post_id, $loader_vars) ): ?>
				<div class="wysiwyg-wrapper-bracket wysiwyg-wrapper-bracket--top"></div>
			<?php endif; ?>
			<div class="wysiwyg-wrapper-content">
				<?php echo apply_filters( 'the_content', do_shortcode($content) ); ?>
			</div>
			<?php if( MiscUtil::get_field_from_loader_vars('wysiwyg_module_brackets', $post_id, $loader_vars) ): ?>
				<div class="wysiwyg-wrapper-bracket wysiwyg-wrapper-bracket--bottom"></div>
			<?php endif; ?>
		<?php endif; ?>
	</div>
</section>