<?php
$id = MiscUtil::get_field_from_loader_vars('posthighlights_grid_id', $post_id, $loader_vars);
$title = MiscUtil::get_field_from_loader_vars('posthighlights_grid_title', $post_id, $loader_vars);
$items = MiscUtil::get_field_from_loader_vars('posthighlights_grid_items', $post_id, $loader_vars);
?>
<section class="posthighlightsgrid section"<?php echo !empty($id) ? ' id="' . $id . '"' : ''; ?>>
	<div data-aos="fade-in" data-aos-offset="-200" class="posthighlightsgrid-wrapper section-wrapper">
		<?php if( !empty($title) ): ?>
			<h1 class="posthighlightsgrid-wrapper-title section-wrapper-title"><?php echo $title; ?></h1>
		<?php endif; ?>
		<?php if( !empty($items) ): ?>
			<div class="posthighlightsgrid-wrapper-items">
				<?php foreach( $items as $item ): ?>
					<a href="<?php echo $item['posthighlights_grid_item_link']['url'] ?>"<?php echo !empty($item['posthighlights_grid_item_link']['target']) ? ' target="' . $item['posthighlights_grid_item_link']['target'] . '"' : ''; ?> class="posthighlightsgrid-wrapper-items-item">
						<div class="posthighlightsgrid-wrapper-items-item-image" style="background-image: url('<?php echo $item['posthighlights_grid_item_bg_image']['sizes']['large']; ?>');"></div>
						<div class="posthighlightsgrid-wrapper-items-item-text" style="background-color: <?php echo $item['posthighlights_grid_item_text_bg_color']; ?>">
							<h3 class="posthighlightsgrid-wrapper-items-item-text-title<?php echo $item['posthighlights_grid_item_split_title_alignment'] ? ' split' : ''; ?>">
								<?php if( !empty($item['posthighlights_grid_item_title1']) ): ?>
									<span class="posthighlightsgrid-wrapper-items-item-text-title-line1"><?php echo $item['posthighlights_grid_item_title1'] ?></span>
								<?php endif; ?>
								<?php if( !empty($item['posthighlights_grid_item_title2']) ): ?>
									<span class="posthighlightsgrid-wrapper-items-item-text-title-line2"><?php echo $item['posthighlights_grid_item_title2'] ?></span>
								<?php endif; ?>
							</h3>
							<?php if( !empty( $item['posthighlights_grid_item_text'] ) ): ?>
								<div class="posthighlightsgrid-wrapper-items-item-text-content">
									<?php echo $item['posthighlights_grid_item_text']; ?>
								</div>
							<?php endif; ?>
						</div>
					</a>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
</section>