<?php
$id = MiscUtil::get_field_from_loader_vars('image_showcase_id', $post_id, $loader_vars);
$images = MiscUtil::get_field_from_loader_vars('image_showcase_gallery', $post_id, $loader_vars);

if( !empty($images) ):
?>
<section class="imageshowcase imageshowcase<?php echo $loader_vars['fc_index']; ?>"<?php echo !empty($id) ? ' id="' . $id . '"' : ''; ?>>
	<div class="imageshowcase-wrapper">
		<div class="imageshowcase-wrapper-images swiper-wrapper">
			<?php foreach( $images as $image ): ?>
				<div class="imageshowcase-wrapper-images-image swiper-slide">
					<div class="imageshowcase-wrapper-images-image-bg" style="background-image: url('<?php echo $image['sizes']['large']; ?>');"></div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>
<?php endif; ?>