<?php
$id = MiscUtil::get_field_from_loader_vars('brackettext_module_id', $post_id, $loader_vars);
$text = MiscUtil::get_field_from_loader_vars('brackettext_module_text', $post_id, $loader_vars);

if( !empty($text) ):
?>
	<section class="brackettextmodule section"<?php echo !empty($id) ? ' id="' . $id . '"' : ''; ?>>
		<div class="brackettextmodule-wrapper section-wrapper">
			<div class="brackettextmodule-wrapper-bracket brackettextmodule-wrapper-bracket--top"></div>
			<div class="brackettextmodule-wrapper-text"><?php echo $text; ?></div>
			<div class="brackettextmodule-wrapper-bracket brackettextmodule-wrapper-bracket--bottom"></div>
		</div>
	</section>
<?php endif; ?>