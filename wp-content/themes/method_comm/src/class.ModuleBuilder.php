<?php 
	
	class ModuleBuilder{
		/**
		 * [$hide_inpage when true the inpage nav doesn't render]
		 * @var boolean
		 */
		public $hide_inpage = false;
		/**
		 * [$name basename of the module buidler fields]
		 * @var string
		 */
		public $name = 'module_builder';
		/**
		 * [$module_partial_relations multidimentional array depicting the module partial relationship and file type]
		 * @var array
		 */
		private $module_partial_relations = array(
			array(
				'slug' => 'wysiwyg_module',
				'file' => 'module.WYSIWYG',
				'type' => 'partial',
			),
			array(
				'slug' => 'banner_module',
				'file' => 'module.Banner',
				'type' => 'partial',
			),
			array(
				'slug' => 'slg_module',
				'file' => 'grid.StaggeredList',
				'type' => 'partial',
			),
			array(
				'slug' => 'accordion_module',
				'file' => 'module.Accordion',
				'type' => 'component',
			),
			array(
				'slug' => 'slider_module',
				'file' => 'module.Slider',
				'type' => 'component',
			),
			array(
				'slug' => 'image_list_grid',
				'file' => 'grid.ImageList',
				'type' => 'partial',
			),
			array(
				'slug' => 'text_list_grid',
				'file' => 'grid.TextList',
				'type' => 'partial',
			),
			array(
				'slug' => 'dynamicgrid_module',
				'file' => 'grid.Dynamic',
				'type' => 'partial',
			),
			array(
				'slug' => 'hrl_module',
				'file' => 'grid.HRList',
				'type' => 'partial',
			),
			array(
				'slug' => 'image_text_grid',
				'file' => 'grid.ImageText',
				'type' => 'partial',
			),
			array(
				'slug' => 'image_showcase',
				'file' => 'module.ImageShowcase',
				'type' => 'partial',
			),
			array(
				'slug' => 'posthighlights_grid',
				'file' => 'grid.PostHighlights',
				'type' => 'partial',
			),
			array(
				'slug' => 'windowpane_grid',
				'file' => 'grid.WindowPane',
				'type' => 'partial',
			),
			array(
				'slug' => 'stretchtofold_module',
				'file' => 'module.StretchToFold',
				'type' => 'partial',
			),
			array(
				'slug' => 'brackettext_module',
				'file' => 'module.BracketText',
				'type' => 'partial',
			),
			array(
				'slug' => 'adaptivetwocolumn_grid',
				'file' => 'grid.AdaptiveTwoColumn',
				'type' => 'partial',
			),
			array(
				'slug' => 'casestudy_grid',
				'file' => 'grid.CaseStudy',
				'type' => 'component',
			),
			array(
				'slug' => 'social_grid',
				'file' => 'grid.Social',
				'type' => 'component',
			),
			array(
				'slug' => 'team_grid',
				'file' => 'grid.Team',
				'type' => 'component',
			),
			array(
				'slug' => 'news_slider',
				'file' => 'slider.News',
				'type' => 'component',
			),
			array(
				'slug' => 'imagelist_module',
				'file' => 'module.ImageList',
				'type' => 'partial',
			),
			array(
				'slug' => 'singlepost_module',
				'file' => 'module.SinglePost',
				'type' => 'partial',
			),
		);

		/**
		 * [__construct when instantiated, it builds out the module builder]
		 * @param array $args (optional) merge's with defaults
		 *                    ID (int) = the post id the module is part of. defaults to global $post's ID
		 *                    artwork_grid_module (array)
		 *                    		from_fc (bool) = from a flexible content?
		 *                    		query_vars (array) = args to pass into WP_Query constructor
		 *                    wysiwyg_module (array)
		 *                    		autop_active (true) = turn on autop
		 *                    		preserve_line_breaks (true) = preserve <br> tags. see: https://codex.wordpress.org/Function_Reference/wpautop
		 *
		 * @param string $name basename for the module builder (defaults to module_builder)
		 * @param str|int $ID the ID of where the module builder lives. For posts use the post's ID. For terms use 'term_' + term_id eg: 'term_123'.
		 */
		public function __construct($args = array(), $name = '', $ID = ''){

			// if ID is empty default to get_queried_object()'s intelligent ID
			if( empty($ID) ){
				$ID = MiscUtil::get_intelligent_id();
			}

			if( !empty($name) ){
				$this->name = $name;
			}

			// set default loader vars per each module
			$defaults = array(
				'ID' => $ID, // could be an int or string like 123 or string like 'term_123' for a term
				'hero_module' => array(
					'from_fc' => true,
					'id' => '',
					'additional_class' => '',
				),
			);

			// merge defaults with args from constructor
			$this->params = array_merge_recursive($defaults, $args);

		}
		/**
		 * [has_modules determines if modules exist or not]
		 * @return boolean true if modules exist and false if modules don't exist
		 */
		public function has_modules(){
			return !empty(get_field($this->name, $this->params['ID']));
		}
		/**
		 * [render spits out HTML for the modules]
		 */
		public function render(){
			// get the layouts
			$layouts = get_field($this->name, $this->params['ID']);

			// did we get layouts?
			if( !empty($layouts) ){
				// loop through layouts
				foreach($layouts as $index => $layout){
					// reconstruct vars & include fc_index
					$vars = @$this->params[$layout['acf_fc_layout']];
					// the "flexible content index" is useful when recalling acf sub fields:
					// get_field(<modulebuildername> . '_' . fc_index . '_' . <subfieldname> );
					$vars['fc_index'] = $index;

					// pass the module builder basename to the partials/components
					$vars['mb_basename'] = $this->name;
					
					// get index in module_partial_relations
					$mpr_index = array_search($layout['acf_fc_layout'], array_column($this->module_partial_relations, 'slug'));
					
					// load the module depending on it's type
					if( $this->module_partial_relations[$mpr_index]['type'] === 'partial' ){
						PartialUtil::get($this->module_partial_relations[$mpr_index]['file'], $vars, $this->params['ID']);
					}
					elseif( $this->module_partial_relations[$mpr_index]['type'] === 'component' ){
						ComponentUtil::get($this->module_partial_relations[$mpr_index]['file'], $vars, $this->params['ID']);
					}
				}
			}
		}
	}

?>