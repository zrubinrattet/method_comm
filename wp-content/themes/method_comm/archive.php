<?php

get_header();

ComponentUtil::get('nav.Main');

PartialUtil::get('hero.Main');

?>
<div class="main">
	<section class="postarchive section">
		<div class="postarchive-wrapper section-wrapper">
			<h1 class="postarchive-wrapper-title section-wrapper-title" data-aos="fade-in">
				<?php
					$qo = get_queried_object();
					if( get_class($qo) === 'WP_User' ){
						echo $qo->data->display_name . '\'s Posts';
					}
					elseif( get_class($qo) === 'WP_Term' ){
						echo $qo->data->name;
					}
				?>
			</h1>
			<?php
			$gp_args = array(
				'post_type' => 'post',
				'posts_per_page' => 10,
				'post_status' => 'publish',
			);
			if( get_class($qo) === 'WP_User' ){
				$gp_args['author'] = $qo->data->ID;
			}
			if( get_class($qo) === 'WP_Term' ){
				$gp_args['tax_query'] = array(
					array(
						'taxonomy' => 'category',
						'field' => 'ID',
						'terms' => $qo->term_id,
					),
				);
			}
			$posts = get_posts($gp_args);

			// reset ppp for total_posts
			$gp_args['posts_per_page'] = -1;

			if( !empty($posts) ): ?>
				<ul class="postarchive-wrapper-grid">
					<?php
					foreach( $posts as $post ){
						PartialUtil::get('griditem.Post', array(
							'post' => $post,
						));
					}
					?>
				</ul>
				<?php if( ($total_posts = count( get_posts($gp_args) )) > 10 ): ?>
					<div class="postarchive-wrapper-seemore">See More</div>
					<input type="hidden" class="postarchive-wrapper-totalposts" value="<?php echo $total_posts; ?>">
				<?php endif; ?>
			<?php endif; ?>
		</div>
	</section>
	<?php
		PartialUtil::get('bg.Parallax');
	?>
</div>
<?php

get_footer();

?>