<?php

get_header();

ComponentUtil::get('nav.Main');

PartialUtil::get('hero.Main');

?>
<div class="main">
	<script type="text/javascript">
		var isPostArchive = true;
	</script>
	<section class="postarchive section">
		<div class="postarchive-wrapper section-wrapper">
			<h1 class="postarchive-wrapper-title section-wrapper-title" data-aos="fade-in">Insights</h1>
			<?php

			global $wpdb;

			$banner_post_ids = array_column($wpdb->get_results("select ID from " . $wpdb->prefix . "posts as p
														join " . $wpdb->prefix . "postmeta as pm
															on pm.post_id = p.ID
														where
															pm.meta_key = 'post_banner'
															and
															p.post_status = 'publish'
															and
															pm.meta_value = 1
														order by
															p.post_title;", ARRAY_A), 'ID');

			// in case there's no banner post ids use a fake one
			if( empty($banner_post_ids) ){
				$banner_post_ids = array(-1);
			}
			$non_banner_post_ids = array_column($wpdb->get_results("select distinct ID from " . $wpdb->prefix . "posts as p
														join " . $wpdb->prefix . "postmeta as pm
															on pm.post_id = p.ID
														where
															p.ID not in (" . implode(',', $banner_post_ids) . ")
															and
															p.post_status = 'publish'
															and
															p.post_type = 'post'
														order by
															p.post_date desc;", ARRAY_A), 'ID');
			// make it an empty array again for the merge
			if( $banner_post_ids[0] === -1 ){
				$banner_post_ids = array();
			}
			$post_ids = array_merge($banner_post_ids, $non_banner_post_ids);

			$posts = array();

			foreach( $post_ids as $post_id ){
				$posts[] = get_post($post_id);
			}


			$total_posts = count( $posts );

			// only show first ten
			$posts = array_slice($posts, 0, 10);

			if( !empty($posts) ): ?>
				<ul class="postarchive-wrapper-grid">
					<?php
					foreach( $posts as $post ){
						PartialUtil::get('griditem.Post', array(
							'post' => $post,
						));
					}
					?>
				</ul>
				<?php if( $total_posts > 10 ): ?>
					<div class="postarchive-wrapper-seemore">See More</div>
					<input type="hidden" class="postarchive-wrapper-totalposts" value="<?php echo $total_posts; ?>">
				<?php endif; ?>
			<?php endif; ?>
		</div>
	</section>
	<?php
		PartialUtil::get('bg.Parallax');
	?>
</div>
<?php

get_footer();

?>